﻿using UnityEngine;
using System.Collections;

public class Life_Feed : MonoBehaviour {

	[SerializeField]float moveVelocity = 0.5f;

	// Use this for initialization
	void Start () {
		//addPosition = transform.position.y;

	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (transform.position.x, transform.position.y + moveVelocity * Time.deltaTime , transform.position.z);
		transform.rotation = Quaternion.LookRotation (transform.position - Camera.main.transform.position);
	}
}
