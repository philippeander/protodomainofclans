﻿//List to Game modes:

//Tutorial 
//Game mode 0: "1 X CPU", Tutorial spawna o player no lado 1, para jogar contra a AI;

//Conpanheiros AI X CONTRA AI
//Game mode 1: 1 x 1
//Game mode 2: 2 X 2
//Game mode 3: 3 X 3

//Companheiros VS AI
//Game mode 4: 1 X 1
//Game mode 5: 2 X 2
//Game mode 6: 3 X 3

//Jogadores VS Jogadores || Ranqueado
//Game mode 7: 1 X 1 
//Game mode 8: 2 X 2
//Game mode 9: 3 X 3
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class GameManager : MonoBehaviour {

    static public GameManager s_Instance = null;

    [Space(10)]
    [Header("TAGs & LAYERs")]
    public string blue_side_Tag = "Blue_Side";
    public string blue_side_BulletTag = "Bullet_Blue_Side";
    public string blue_side_BaseTag = "Base_Blue_Side";
    public int blue_side_LayerMask = 10;

    [Space(10)]
    public string red_side_Tag = "Red_Side";
    public string red_side_BulletTag = "Bullet_Red_Side";
    public string red_side_BaseTag = "Base_Red_Side";
    public int red_side_LayerMask = 11;

    [Space(10)]
    [Header("UI OBJECTs")]
	[SerializeField]Text txtPcTime;
	[SerializeField]Text txtTimer;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] Text gameOverTxt;
    [SerializeField] Text txtInfos;

    public Transform[] playerStartPoints_BlueSide;
    public Transform[] playerStartPoints_RedSide;

    //[Header("Gameplay")]

    [Space(20)]
	[SerializeField]List<GameObject> listOfAllChar;


	//PlayerManager playerManager;
	float timer;

	public static int playerSide = 0; // Pega o lado do campo de acordo com o time
	public static int gameMode = 0;	// Pega o modo de jogo escolhido pelo jogador
	public static int spawnNum = 0;	// Pega qual a posição vai se espanado
	public static int charId = 0;	// Pega qual personagem foi escolhido

	public List<GameObject> ListOfAllChar{get{return listOfAllChar;}set{listOfAllChar = value;}}

    public GameObject GameOverScreen
    {
        get
        {
            return gameOverScreen;
        }

        set
        {
            gameOverScreen = value;
        }
    }
    public Text GameOverTxt
    {
        get
        {
            return gameOverTxt;
        }

        set
        {
            gameOverTxt = value;
        }
    }

    public Text TxtInfos
    {
        get
        {
            return txtInfos;
        }

        set
        {
            txtInfos = value;
        }
    }

    void Awake(){
        if (!gameOverScreen) {
            gameOverScreen = GameObject.Find("Canvas/GameOver");
            gameOverTxt = GameObject.Find("Canvas/GameOver/txt_GameOver").GetComponent<Text>();
        }

        if(!txtInfos)
            GameObject.FindGameObjectWithTag("Canvas/TextInfos").GetComponent<Text>();

    }


    void Start () {
		//Cursor.visible = false;
		timer = 0;
        gameOverScreen.SetActive(false);
        txtInfos.text = "";
    }


	void Update () {
		//txtPcTime.text = DateTime.Now.ToString ("yyyy-MM-dd_HH:mm:ss");
		txtPcTime.text = DateTime.Now.ToString ("HH:mm tt");
		TimerPartida ();

	}


	void TimerPartida(){
		timer += Time.deltaTime;
		txtTimer.text = string.Format ("{0:0}:{1:00}", Mathf.Floor(timer/60), timer%60);
	}

    public void SetCharProperties_Blue_Side(CharTags obj) {
        
        obj.MyTag = blue_side_Tag;
        obj.MyBulletTag = blue_side_BulletTag;
        obj.MyBaseTag = blue_side_BaseTag;
        obj.MyLayerMask = blue_side_LayerMask;

        obj.EnemyTag = red_side_Tag;
        obj.EnemyBulletTag = red_side_BulletTag;
        obj.EnemyBaseTag = red_side_BaseTag;
        obj.EnemyLayerMask = red_side_LayerMask;

    }
    public void SetCharProperties_Red_Side(CharTags obj)
    {

        obj.MyTag = red_side_Tag;
        obj.MyBulletTag = red_side_BulletTag;
        obj.MyBaseTag = red_side_BaseTag;
        obj.MyLayerMask = red_side_LayerMask;

        obj.EnemyTag = blue_side_Tag;
        obj.EnemyBulletTag = blue_side_BulletTag;
        obj.EnemyBaseTag = blue_side_BaseTag;
        obj.EnemyLayerMask = blue_side_LayerMask;

    }

}
