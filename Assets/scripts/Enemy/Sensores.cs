﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class Sensores : NetworkBehaviour {


	//As Variaveis abaixo são semelhantes para que ele sempre ataque o inimigo mais proximo
	[Space(10)]
	[Header("SENSOR AREA")]
	[SerializeField] float distSensorArea = 15f; // Raio para detectar e perseguir inimigos
	public Transform targetSensorArea;

	[Space(10)]
	[Header("DIST TO SHOOT")]
	[SerializeField] float distToShoot = 5f; // Raio para atacar, se usar armas de Tiro
    public Transform targetToShoot;

	[Space(10)]
	[Header("DIST TO PHYSICAL ATTACK")]
	[SerializeField] float distToPhysAttack = 3f; // Raio para atacar ataques Fisicos
    public Transform targetToAttack;

	[Space(10)]
	[Header("DIST TO RECOVER TOWER (Only to Player Character)")]
	[SerializeField] float distToCharacters = 3f; // Raio para recuperação da torre
    public Transform targetCharacters;

	public GameObject[] enemies = new GameObject[0];
	protected GameManager gameManager;
	[SerializeField]protected string enemieTag;
	public bool inSight;
	protected int enemyLayer;
    protected Vector3 finalPoint;

    protected CharTags charProp;

    public Transform TargetToShoot{get{return targetToShoot;}set{targetToShoot = value;}}

	public virtual void Awake(){
        
        gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();

	}
	// Use this for initialization
	public virtual void Start () {
        
        charProp = GetComponent<CharTags>();

        enemieTag = charProp.EnemyTag;
		enemyLayer = charProp.EnemyLayerMask;

        InvokeRepeating("enemyListUpdate", 0f, 2f);
		

	}
    

    // Update is called once per frame
    public virtual void Update () {
       
		SensorArea ();
		DistToshoot ();
		DistToPhysAttack ();
	}

	public virtual void FixedUpdate(){
        
        VerifyForward ();
	}

    //============================
    void enemyListUpdate()
    {
        enemies = new GameObject[0];

        int cont = 1;
        for (int i = 0; i < gameManager.ListOfAllChar.Count; i++)
        {
            if (gameManager.ListOfAllChar[i].CompareTag(enemieTag))
            {
                Array.Resize(ref enemies, cont);
                enemies[cont - 1] = gameManager.ListOfAllChar[i];
                cont++;
            }
        }
    }

	void VerifyForward(){
		//Quaternion spreadAngle_n = Quaternion.AngleAxis(-45, new Vector3(0, 1, 0));

		Ray ray = new Ray (new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z), transform.forward);
		Debug.DrawRay (new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z), transform.forward * 20.0f, Color.cyan );
		//Vector3 endPointRaycast  = ray.origin + (ray.direction * 20f);
		RaycastHit hitInfo;
		if (Physics.Raycast (ray, out hitInfo, 20.0f)) {

			if (hitInfo.collider.tag == enemieTag) {
				inSight = true;
                finalPoint = hitInfo.point;
			} else {
				inSight = false;
			}
		}
	}

	//<<<<<<<<<<<<<<<<<<<<< SENSORES >>>>>>>>>>>>>>>>>>>>>>>
	//Para evitar usar gatilhos atraves de collisores, foi-se usado Vector3.Distance
	//Ele pega todos os inimigos atraves da teg e joga dentro de um array.

	//1º SENSOR
	void SensorArea(){
		float shortesDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		for(int i = 0; i < enemies.Length; i++){
			if (enemies[i].activeSelf) {
				float distanceToEnemy = Vector3.Distance (transform.position, enemies[i].transform.position);
				if (distanceToEnemy < shortesDistance) {
					shortesDistance = distanceToEnemy;
					nearestEnemy = enemies[i];
				}
			}
		}
        //Posso replicar esse meto e as unicas variaveis serem ajustadas são:
        if (nearestEnemy != null && shortesDistance <= distSensorArea)
        {   //<<<
            targetSensorArea = nearestEnemy.transform;	//<<<
        }
        else
        {
            targetSensorArea = null; //<<<	
        }

	}

	//2º SENSOR
	void DistToshoot(){
		float shortesDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		for(int i = 0; i < enemies.Length; i++){
			if (enemies[i].activeSelf) {
				float distanceToEnemy = Vector3.Distance (transform.position, enemies[i].transform.position);
				if (distanceToEnemy < shortesDistance) {
					shortesDistance = distanceToEnemy;
					nearestEnemy = enemies[i];
				}
			}
		}
		//Posso replicar esse meto e as unicas variaveis serem ajustadas são:
		if (nearestEnemy != null && shortesDistance <= distToShoot) 	//<<<
			targetToShoot = nearestEnemy.transform;	//<<<
		else
			targetToShoot = null; //<<<										
	}

	//3º SENSOR
	void DistToPhysAttack(){
		float shortesDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		for(int i = 0; i < enemies.Length; i++){
			if (enemies[i].activeSelf) {
				float distanceToEnemy = Vector3.Distance (transform.position, enemies[i].transform.position);
				if (distanceToEnemy < shortesDistance) {
					shortesDistance = distanceToEnemy;
					nearestEnemy = enemies[i];
				}
			}
		}
		//Posso replicar esse meto e as unicas variaveis serem ajustadas são:
		if (nearestEnemy != null && shortesDistance <= distToPhysAttack) 	//<<<
			targetToAttack = nearestEnemy.transform;	//<<<
		else
			targetToAttack = null; //<<<										
	}
    
	//Desenha o Raio usado para melhor visualizar na edição
	void OnDrawGizmosSelected(){
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, distSensorArea);
		Gizmos.DrawWireSphere (transform.position, distToShoot);
		Gizmos.DrawWireSphere (transform.position, distToPhysAttack);
		Gizmos.DrawWireSphere (transform.position, distToCharacters);

	}



}


