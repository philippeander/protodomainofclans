﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shot : NetworkBehaviour {
    [Space(10)]
    public string addressPolling;
    public float bulletDamage = 10f;
    public float bulletSpeed = 30;

    [Space(10)]
    public bool haveAnimation = false;
    public float timeToDoesNotAnim = 1f;

    [Space(10)]
    public Transform[] spawnerPoints;

    string bulletTag;
    string enemyTag;
    Vector3 finalPoint;

    [SyncVar][SerializeField]bool m_shot;

    CharTags charTags;
    NetworkedPool bulletContainer;
    Animator anim;

    //====================================

    private void Awake()
    {
        anim = GetComponent<Animator>();

    }

    private void Start()
    {
        charTags = GetComponent<CharTags>();

        bulletTag = charTags.MyBulletTag;
        enemyTag = charTags.EnemyTag;
    }

    public override void OnStartServer()
    {
        bulletContainer = GameObject.Find(addressPolling).GetComponent<NetworkedPool>();

        InvokeRepeating("NoAnimation", 0f, timeToDoesNotAnim);
    }

    private void Update()
    {
        Anim();
        
    }

    //========= INPUT ===============================
    //Para personagens que não usam animações no evento de atirar
    public void ShootEnemy(Vector3 target)
    {
        if (!isServer) return;
        finalPoint = target;
        m_shot = true;
    }

    //Ele fica chamando enquanto m_Shot = true em tempos palsados no InvokeRepeating
    void NoAnimation()
    {
        if (!isServer) return;

        if (!haveAnimation && m_shot)
        {
            IsShoot();
        }
    }

    //======= ANIMATION ========

    //Seta as animações de tiro, para que os eventos sejam execultados
    void Anim()
    {
        if (anim && haveAnimation)
            anim.SetBool("Shoot", m_shot);
    }

    //E chamdo como evento de animação
    void AnimEventShoot()
    {
        IsShoot();
    }
    
    //======================================
    
    
    void IsShoot()
    {
        for (int i = 0; i < spawnerPoints.Length; i++)
        {
            GameObject l_bulllet = bulletContainer.GetFromPool(spawnerPoints[i].position);

            //l_bulllet.transform.LookAt(finalPoint);
            l_bulllet.transform.rotation = spawnerPoints[i].rotation;

            l_bulllet.tag = bulletTag;

            BulletLife BL = l_bulllet.GetComponent<BulletLife>();
            BL.ObjQueDisparou = this.gameObject;
            BL.EnemyTag = enemyTag;
            BL.BulletValue = bulletDamage;

            l_bulllet.GetComponent<Rigidbody>().velocity = l_bulllet.transform.forward * bulletSpeed;

            NetworkServer.Spawn(l_bulllet, bulletContainer.assetId);

        }

        m_shot = false;
    }
    
}
