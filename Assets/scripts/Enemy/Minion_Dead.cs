﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Minion_Dead : NetworkBehaviour {

    Health healthScript;
    NetworkedPool objManager;

    public NetworkedPool ObjManager
    {
        get
        {
            return objManager;
        }

        set
        {
            objManager = value;
        }
    }

    private void Awake()
    {
        healthScript = GetComponent<Health>();

        healthScript.EventDie += DisableObject;
    }
    

    public override void OnNetworkDestroy()
    {
        healthScript.EventDie -= DisableObject;
    }

    void DisableObject()
    {
        healthScript.RegenerateOnly();
        healthScript.ShouldDie = false;

        if (objManager) {
            objManager.UnSpawnObject(this.gameObject);
            NetworkServer.UnSpawn(this.gameObject);
        }
        
    }
}
