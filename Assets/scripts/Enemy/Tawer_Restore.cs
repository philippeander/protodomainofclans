﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tawer_Restore : NetworkBehaviour
{
    //Player_UIManager uiManager;
    Health healthScript;

    // Use this for initialization
    void Awake () {
        healthScript = GetComponent<Health>();

        healthScript.EventRespawn += EnablePlayer;
    }

    public override void OnNetworkDestroy()
    {
        healthScript.EventRespawn -= EnablePlayer;
    }

    void EnablePlayer()
    {
        if (healthScript.IsDead)
        {
            healthScript.ResurrectCountDown -= Time.deltaTime;
            /*
            if (uiManager)
            {
                uiManager.TxtActiveCount.text = string.Format("{0:0}:{1:00}", Mathf.Floor(healthScript.ResurrectCountDown / 60), healthScript.ResurrectCountDown % 60) + " / " +
                    string.Format("{0:0}:{1:00}", Mathf.Floor(healthScript.ResurrectMaxValue / 60), healthScript.ResurrectMaxValue % 60) + "\nto Ressurrect";

            }
            */
        }
    }
}
