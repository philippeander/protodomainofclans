﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tawer_Dead : NetworkBehaviour
{

    Health healthScript;

    private void Awake()
    {
        healthScript = GetComponent<Health>();

        healthScript.EventDie += DisablePlayer;
    }

    public override void OnNetworkDestroy()
    {
        healthScript.EventDie -= DisablePlayer;
    }

    void DisablePlayer()
    {
        gameObject.SetActive(false);
        /*
        GetComponent<TowerAi>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().enabled = false;

        Renderer[] renderes = GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderes.Length; i++)
        {
            renderes[i].enabled = false;
        }
        */
        //healthScript.IsDead = true;

        //healthScript.RegenerateOnly();

        //healthScript.ShouldDie = false;

    }
}
