﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy_UiManager : MonoBehaviour {

    [SerializeField] Canvas m_canvas;
    [SerializeField] Slider lifeSlide;
    [SerializeField] Color normalLife = Color.green;
    [SerializeField] Color critialLife = Color.red;

    Health healthScript;
    ColorBlock cb;

    private void Awake()
    {
        healthScript = GetComponent<Health>();
        cb = lifeSlide.colors;
    }

    void Start () {
        lifeSlide.maxValue = healthScript.Life_MaxValue;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        m_canvas.transform.LookAt(m_canvas.transform.position + Camera.main.transform.rotation * Vector3.back,
                                    Camera.main.transform.rotation * Vector3.up);

        lifeSlide.value = healthScript.LifeCurrent;

        if (healthScript.LifeCurrent < healthScript.Life_MaxValue * (40f / 100f))
        {
            cb.normalColor = critialLife;
        }
        else {
            cb.normalColor = normalLife;

        }

        lifeSlide.colors = cb;
    }
}
