﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {

    //Valures que incrementam os valores Fixos nas atualizações
    [Header("UPDATE VALUES")]
    [SerializeField] float lifeUpdate = 20f;
    [SerializeField] float energyUpdate = 20f;
    [SerializeField] float resurrectUpdate = 6f;

    //Valores fixos
    [Space(10)]
    [Header("FIXED VALUES")]
    [SerializeField] [SyncVar (hook = "OnLife_MaxChanged")] float life_MaxValue = 100f;
    [SerializeField] [SyncVar (hook = "OnEnergy_MaxChanged")] float energy_MaxValue = 100f;
    [SerializeField] [SyncVar (hook = "OnResurrect_MaxChanged")] float resurrectMaxValue = 12;

    [Space(10)]
    [Header("CURRENT VALUES")]
    [SerializeField] [SyncVar(hook = "OnLifeCurrentChanged")] float lifeCurrent;
    [SerializeField] [SyncVar(hook = "OnEnergyCurrentChanged")] float energyCurrent;
    [SerializeField] [SyncVar(hook = "OnRessurrectCountChanged")] float resurrectCountDown;

    [Space(10)]
    [Header("CONDITIONS")]
    [SerializeField] bool isEvolves = false;    //Se o personagem evolui, a medida que mata
    [SerializeField] bool haveEnergy = false;   //Se o Char perde energia ao usar uma ataque
    [SerializeField] bool regenerate = false;   //Regenera o Life e Aenergia
    [SerializeField] bool invincible = false;   //Se perde life quando atacado
    [SerializeField] bool DamageFeed = false;   //Se spawna o valor do Golpe quando leva dano
    [SerializeField] bool returnToInitialPos = false;


    [Space(10)]
    [Header("OBJECTS")]
    [SerializeField] GameObject DamageFeed_Prefab;


    CharTags charProp;
    
    GameObject baseSide;
    GameObject enemyLastAttack;

    string baseTag;

    [SyncVar (hook = "OnPlayerLevelChanged")] int playerLevel = 0; //Level do Player, evolução

    bool shouldDie;
    bool isDead;
    bool isBase;


    public float LifeCurrent
    {
        get
        {
            return lifeCurrent;
        }

        set
        {
            lifeCurrent = value;
        }
    }
    public float EnergyCurrent
    {
        get
        {
            return energyCurrent;
        }

        set
        {
            energyCurrent = value;
        }
    }
    public float Life_MaxValue
    {
        get
        {
            return life_MaxValue;
        }

        set
        {
            life_MaxValue = value;
        }
    }
    public float Energy_MaxValue
    {
        get
        {
            return energy_MaxValue;
        }

        set
        {
            energy_MaxValue = value;
        }
    }
    public bool ShouldDie
    {
        get
        {
            return shouldDie;
        }

        set
        {
            shouldDie = value;
        }
    }
    public bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }
    public bool IsEvolves
    {
        get
        {
            return isEvolves;
        }

        set
        {
            isEvolves = value;
        }
    }
    public bool ReturnToInitialPos
    {
        get
        {
            return returnToInitialPos;
        }

        set
        {
            returnToInitialPos = value;
        }
    }
    public int PlayerLevel
    {
        get
        {
            return playerLevel;
        }

        set
        {
            playerLevel = value;
        }
    }
    public float ResurrectMaxValue
    {
        get
        {
            return resurrectMaxValue;
        }

        set
        {
            resurrectMaxValue = value;
        }
    }
    public float ResurrectCountDown
    {
        get
        {
            return resurrectCountDown;
        }

        set
        {
            resurrectCountDown = value;
        }
    }
    public float LifeUpdate
    {
        get
        {
            return lifeUpdate;
        }

        set
        {
            lifeUpdate = value;
        }
    }
    public float EnergyUpdate
    {
        get
        {
            return energyUpdate;
        }

        set
        {
            energyUpdate = value;
        }
    }

    public delegate void DieDelegate();
    public event DieDelegate EventDie;

    public delegate void RespawnDelegate();
    public event RespawnDelegate EventRespawn;

    void Awake()
    {
        if (DamageFeed)
            DamageFeed_Prefab = (GameObject)Resources.Load("LifeExtract_Feed", typeof(GameObject));

        charProp = GetComponent<CharTags>();

    }

    void Start()
    {
        lifeCurrent = life_MaxValue;
        energyCurrent = energy_MaxValue;
        resurrectCountDown = resurrectMaxValue;
        isDead = false;
        shouldDie = false;
        isBase = false;

        if (regenerate)
            baseSide = GameObject.FindGameObjectWithTag(charProp.MyBaseTag);

        if(regenerate)
            InvokeRepeating("BaseDistanceToRegenerate", 0.5f, 0.5f);

    }
    
    //
    private void Update()
    {
        CheckCondition();

        if (regenerate)
        {
            //BaseDistanceToRegenerate();
            LifeAddByTime();
            if (haveEnergy)
                EnergyAddByTime();

        }

    }

    //Chamado pelo Script que difere o Attack
    public void OnDamage(float amount, GameObject objQueDisparou)
    {
        if (!isServer) return;
        if (invincible) return;
        if (shouldDie) return;
        if (amount <= 0) return;

        enemyLastAttack = objQueDisparou;
        lifeCurrent -= amount;

        if (DamageFeed)
        {
            GameObject instFeed = Instantiate(DamageFeed_Prefab, new Vector3(transform.position.x, transform.position.y + 1.8f, transform.position.z), Quaternion.identity, gameObject.transform) as GameObject;
            instFeed.GetComponentInChildren<TextMesh>().text = amount.ToString();
            Destroy(instFeed, 2f);

        }

        

    }

    void CheckCondition()
    {
        FixedValues();

        if (lifeCurrent <= 0 && !shouldDie && !isDead)
        {
            shouldDie = true;
        }

        if (lifeCurrent <= 0 && shouldDie)
        {
            if (EventDie != null)
            {
                EventDie();
            }

            //shouldDie = false;
            
            //RpcIfUpdateEnemyValues(); //Faz o update do objeto que atirou
            //RegenetareAndUpdateValues(); //Regenera e faz o update dos valores dos players e guardiões
            
        }

        if (lifeCurrent > 0 && isDead)
        {
            if (EventRespawn != null)
            {
                EventRespawn();
            }
            
        }
    }

    //Quando isDead = true
    //Aumenta a vida e Energia do Inimigo
    //Incrementa O level do Player que atacou
    [ClientRpc]
    public void RpcIfUpdateEnemyValues() {

        if (isLocalPlayer) // <<<<<<<<<
        {    
            //Parte responsavel pelo incremento do Tempo de Ressussitação do Player apos Morrer
            Health healthEnemy = enemyLastAttack.GetComponent<Health>();
            if (healthEnemy)
            {
                if (isEvolves && healthEnemy.IsEvolves)
                {
                    healthEnemy.playerLevel += 1;
                    healthEnemy.life_MaxValue += healthEnemy.LifeUpdate;
                    healthEnemy.energy_MaxValue += healthEnemy.EnergyUpdate;

                }
            }
        }

        enemyLastAttack = null;
    }
    public void RegenetareAndUpdateValues()
    {
        lifeCurrent = life_MaxValue;
        energyCurrent = energy_MaxValue;
        resurrectMaxValue += resurrectUpdate;
        resurrectCountDown = resurrectMaxValue;

    }
    public void RegenerateOnly()
    {
        lifeCurrent = life_MaxValue;
    }

    void LifeAddByTime()
    {
        if (lifeCurrent < life_MaxValue && lifeCurrent > 1) {
            if (isBase)
            {   //Se o o player estiver na Base
                lifeCurrent += Time.deltaTime * 5f;
            }
            else 
            {   //Se o player "NÃO" estiver na Base
                lifeCurrent += Time.deltaTime / 2.5f;

            }
        }
    }
    void EnergyAddByTime()
    {
        if (energyCurrent < energy_MaxValue && energyCurrent > 1)
        {
            if (isBase)
            {           //Se o o player estiver na Base
                energyCurrent += Time.deltaTime * 5f;
            }
            else
            {   //Se o player "NÃO" estiver na Base
                energyCurrent += Time.deltaTime / 2.5f;
            }
        }
    }
    void BaseDistanceToRegenerate()
    {     
        if (Vector3.Distance(transform.position, baseSide.transform.position) <= baseSide.GetComponent<BaseRecover>().RadiusArea)
        {
            isBase = true;
            /*
            if (lifeCurrent < life_MaxValue)
            {
                carregandoLifeBase = true;
            }
            */
        }
        else
        {
            isBase = false;
            /*
            if (lifeCurrent >= life_MaxValue)
            {
                carregandoLifeBase = false;
            }
            */
        }
    }
    void FixedValues()
    {
        if (lifeCurrent > life_MaxValue)
        {
            lifeCurrent = life_MaxValue;
        } else if (lifeCurrent < 0) {
            lifeCurrent = 0;
        }
        if (haveEnergy)
        {
            if (energyCurrent > energy_MaxValue)
            {
                energyCurrent = energy_MaxValue;
            } else if (energyCurrent < 0) {
                energyCurrent = 0;
            }
        }
    }

    

    //---------------- VVV SYNCVAR (HOOK = "") VVV ----------------------------

    void OnPlayerLevelChanged(int val)
    {
        playerLevel = val;
    }

    void OnLife_MaxChanged(float val)
    {
        life_MaxValue = val;
    }
    void OnEnergy_MaxChanged(float val)
    {
        energy_MaxValue = val;
    }
    void OnResurrect_MaxChanged(float val) {
        ResurrectMaxValue = val;
    }

    void OnLifeCurrentChanged(float val) {
        lifeCurrent = val;
    }
    void OnEnergyCurrentChanged(float val) {
        energyCurrent = val;
    }
    void OnRessurrectCountChanged(float val){
        resurrectCountDown = val;
    }


    //ATENÇÃO: A EXEMPLO DA TORRE, é ela que fica com esse metodo, e o sensor dela que pega o player para playerToRecover
    public void RecoverByInput(GameObject playerToRecover)
    {//Pode ser um met publico para ser acessado pela classe principal

        Health playerHealth = playerToRecover.GetComponent<Health>();

        if (Input.GetKey(KeyCode.R))
        {

            if (playerHealth.LifeCurrent > 0)
            {
                float val = Time.deltaTime / 2f;

                this.lifeCurrent += val; //Aqui é o life da torre

                playerHealth.OnDamage(val, null);

            }

        }

    }
}
