﻿using UnityEngine;
using UnityEngine.Networking;


public class Shooting : NetworkBehaviour {

	enum BulletType{
		Tower,
		Guardian,
		Hunter
    
	}

	[SerializeField] BulletType bulletType;

	[Space(15)]
	[Header("BULLET OBJECT POOLING")]
	[SerializeField] GameObject bullet;
	[SerializeField] int poolBulletNum = 6;
    [SerializeField] float bulletDamage = 15;

	
	BulletManager bulletManager;
	[SerializeField] Transform bulletContainerObj;

	[Space(10)]
	[SerializeField] Transform[] spownerPoint;
	[SerializeField]float bulletSpeed = 30f;
	[SerializeField]bool haveAnimator = false;
	[SerializeField]float timeToDontHaveAnim = 1f;

	//Transform target;
	Animator anim;
	bool m_shoot;
    string bulletTag;
    string enemyTag;

    CharTags charProp;

    void Awake(){
		anim = GetComponent<Animator> ();
	}

	void Start () {
        charProp = GetComponent<CharTags>();

        bulletTag = charProp.MyBulletTag;
        enemyTag = charProp.EnemyTag;

		bulletManager = GameObject.Find ("GameManager").GetComponent<BulletManager> ();

        CmdInstBullets();

		InvokeRepeating ("NoAnimations", 0f, timeToDontHaveAnim);
	}

	void Update () {
		
		Anim ();
	}

    //Seta as animações de tiro, para que os eventos sejam execultados
    void Anim()
    {
        if (anim && haveAnimator)
            anim.SetBool("Shoot", m_shoot);
    }

    [Command]
    void CmdInstBullets()
    {

        switch (bulletType)
        {
            case BulletType.Guardian:
                bulletContainerObj = GameObject.Find("CharsList/m_Bullets/B_Guardian").transform;
                break;
            case BulletType.Hunter:
                bulletContainerObj = GameObject.Find("CharsList/m_Bullets/B_Hunter").transform;
                break;
            case BulletType.Tower:
                bulletContainerObj = GameObject.Find("CharsList/m_Bullets/B_Tower").transform;
                break;
        }
        
        for (int i = 0; i < poolBulletNum; i++)
        {
            GameObject inst = Instantiate(bullet, transform.position, transform.rotation) as GameObject;

            inst.transform.parent = bulletContainerObj;
            inst.transform.position = Vector3.zero;

            switch (bulletType)
            {
                case BulletType.Guardian:
                    bulletManager.GuardianBullets.Add(inst);
                    break;
                case BulletType.Hunter:
                    bulletManager.HunterBullets.Add(inst);
                    break;
                case BulletType.Tower:
                    bulletManager.TowerBullets.Add(inst);
                    break;
            }

            inst.SetActive(false);
        }
    }

    //---------------- RECEBE OS VALORES DO INPUT no "Player Controlers"---------------------------

    public void ShootEnemy(Transform enemy){
		//target = enemy;
		m_shoot = true;
	}

    //----------------- INICIA O TIRO --------------------------

    // Esse evento é para os objetos "SEM" animações de Tiro, e é chamado no InvokeRepinting no metodo Start
    void NoAnimations(){ 
		if(!haveAnimator && m_shoot){
			IsShoot ();
		}
	}
    
	// Esse evento é para os objetos "COM" animações de Tiro
	// Ele é chamado a partir de um evento na propria animação na Window "Animation"
	void AnimEventShoot(){ 
		IsShoot ();
	}

    //-------------------------------------------

    void IsShoot(){
		for (int i = 0; i < spownerPoint.Length; i++) {
			
			GameObject l_Bullet = GetBullet();

			l_Bullet.tag = bulletTag;

			l_Bullet.SetActive (true);
			l_Bullet.transform.position = spownerPoint[i].position;

			SetBulletParamiters (l_Bullet);
//			if (bulletType == BulletType.PlayerPassiva) {
//				if (GetComponent<AiPlayerController>().enabled) {
//					l_Bullet.transform.LookAt (GetComponent<AiPlayerController>().TargetToShoot.transform.position);
//				} else {
//					l_Bullet.transform.LookAt (Camera.main.gameObject.GetComponent<CameraController>().Target);
//					//l_Bullet.GetComponent<Rigidbody> ().velocity = l_Bullet.transform.forward * 100.0f;
//				}
//			} else {
//				l_Bullet.transform.LookAt (new Vector3 (target.position.x, target.position.y + 0.8f, target.position.z));
//			}
			l_Bullet.GetComponent<Rigidbody> ().velocity = l_Bullet.transform.forward * bulletSpeed;

		}

		m_shoot = false;
	}

    GameObject GetBullet()
    {
        GameObject objBullet = null;

        switch (bulletType)
        {
            case BulletType.Guardian:
                objBullet = bulletManager.GuardianBullets[0];
                bulletManager.Reload_GuardianBullet(objBullet);
                break;
            case BulletType.Hunter:
                objBullet = bulletManager.HunterBullets[0];
                bulletManager.Reload_HunterBullet(objBullet);
                break;
            case BulletType.Tower:
                objBullet = bulletManager.TowerBullets[0];
                bulletManager.Reload_TowerBullet(objBullet);
                break;
        }

        return objBullet;
    }

    void SetBulletParamiters(GameObject bullet){
        BulletLife BL = bullet.GetComponent<BulletLife>();

        BL.ObjQueDisparou = this.gameObject;
        BL.EnemyTag = enemyTag;
        BL.BulletValue = bulletDamage;
	}

	

    
}
