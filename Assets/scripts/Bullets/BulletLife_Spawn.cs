﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Networking;
using UnityEngine;

public class BulletLife_Spawn : MonoBehaviour
{

    [SerializeField] GameObject objQueDisparou;//Variavel relacionada ao objeto que disparou a bala. Para que possa transferir recompensa
    [SerializeField] bool destroyOnCollision = false;

    public string enemieTag = "";
    float bulletDamage;//Valor relacionado ao dano no objeto inimigo


    public float BulletValue { get { return bulletDamage; } set { bulletDamage = value; } }
    public GameObject ObjQueDisparou { get { return objQueDisparou; } set { objQueDisparou = value; } }
    public string EnemyTag { set { enemieTag = value; } }
    

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Untagged"))
        {

            if (destroyOnCollision) DeactiveBullet();
        }

        if (other.transform.tag == enemieTag)
        {

            if (other.GetComponent<Health>())
            {

                other.GetComponent<Health>().OnDamage(bulletDamage, objQueDisparou);

            }

            if (destroyOnCollision) DeactiveBullet();
        }

    }

    void DeactiveBullet()
    {
        Destroy(gameObject);
    }
}