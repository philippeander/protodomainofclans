﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

public class BulletLife : NetworkBehaviour {

	[SerializeField]GameObject objQueDisparou;//Variavel relacionada ao objeto que disparou a bala. Para que possa transferir recompensa
    [SerializeField]float lifeSpan = 1.0f;
    [SerializeField] bool destroyOnCollision = false;

	public string enemieTag = "";
    float startTime;
    float bulletDamage;//Valor relacionado ao dano no objeto inimigo

    NetworkedPool spawnManager;
    
    public float BulletValue{get{return bulletDamage;}set{bulletDamage = value;}}
	public GameObject ObjQueDisparou{get{return objQueDisparou;}set{objQueDisparou = value;}}
    public string EnemyTag { set { enemieTag = value; } }
    public NetworkedPool SpawnManager
    {
        get
        {
            return spawnManager;
        }

        set
        {
            spawnManager = value;
        }
    }
    
    
    void OnEnable(){
        startTime = Time.timeSinceLevelLoad;
	}

	void Update(){

        if (!isServer) return;
        
        CheckLifeSpan();

	}

    private void CheckLifeSpan()
    {
        if (Time.timeSinceLevelLoad - startTime > lifeSpan) {
            //This sets the object inactive for the pool to reuse
            DeactiveBullet();
        }
    }

    void OnTriggerEnter(Collider other) {

		if(other.CompareTag("Untagged")){
            
            if (destroyOnCollision) DeactiveBullet();
        }

		if (other.transform.tag == enemieTag) {
			
			if(other.GetComponent<Health>()){

				other.GetComponent<Health> ().OnDamage (bulletDamage, objQueDisparou);
                
			}

            if (destroyOnCollision) DeactiveBullet();
		}

	}

    void DeactiveBullet()
    {
        transform.tag = "Untagged";
        enemieTag = "";
        spawnManager.UnSpawnObject(this.gameObject);
        NetworkServer.UnSpawn(this.gameObject);
        


    }
}

