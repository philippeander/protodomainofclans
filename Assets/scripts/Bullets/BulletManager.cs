﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Networking;

public class BulletManager : MonoBehaviour {

	[SerializeField]List<GameObject> guardianBullets;
	[SerializeField]List<GameObject> hunterBullets;
	[SerializeField]List<GameObject> towerBullets;
	[SerializeField]List<GameObject> playerPassiva;

	public List<GameObject> GuardianBullets{get{return guardianBullets;}set{guardianBullets = value;}}
	public List<GameObject> HunterBullets{get{return hunterBullets;}set{hunterBullets = value;}}
	public List<GameObject> TowerBullets{get{return towerBullets;}set{towerBullets = value;}}
	public List<GameObject> PlayerPassiva{get{return playerPassiva;} set{playerPassiva = value;}}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Reload_GuardianBullet(GameObject bullet){ //As balas que estiverem ativas no indice 0, seram realocadas apara o indice -1 da lista
		guardianBullets.RemoveAt (0);
		guardianBullets.Add (bullet);
	}

	public void Reload_TowerBullet(GameObject bullet){ //As balas que estiverem ativas no indice 0, seram realocadas apara o indice -1 da lista
		towerBullets.RemoveAt (0);
		towerBullets.Add (bullet);
	}

	public void Reload_HunterBullet(GameObject bullet){ //As balas que estiverem ativas no indice 0, seram realocadas apara o indice -1 da lista
		hunterBullets.RemoveAt (0);
		hunterBullets.Add (bullet);
	}

	public GameObject Reload_PlayerPassBullet(){ //As balas que estiverem ativas no indice 0, seram realocadas apara o indice -1 da lista

        GameObject bullet = playerPassiva[0];

        playerPassiva.RemoveAt (0);
		playerPassiva.Add (bullet);

        return bullet;
	}
		
}
