﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Guardian_Setup : NetworkBehaviour {
    
	void Start () {
        if (isServer)
        {
            GetComponent<GuardianAi>().enabled = true;
            GetComponent<Shot>().enabled = true;
        }
	}
	
}
