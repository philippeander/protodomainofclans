﻿using UnityEngine;
using System.Collections;

public class BarrierCollider : MonoBehaviour {

	bool m_BarrierCollider; 

	public bool Barrier{get{return m_BarrierCollider;}}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Barrier"){
			m_BarrierCollider = true;
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == "Barrier") {
			m_BarrierCollider = false;
		}
	}
}
