﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(NavMeshObstacle))]
[RequireComponent(typeof(Rigidbody))]
public class GuardianAi : Sensores {



	[SerializeField]float MovingTurnSpeed = 360.0f;
	//[SerializeField] GameObject[] othersObjects = null;
	//[SerializeField] Collider[] othersColliders = null;

	Animator anim;
	Vector3 initialPoint;
	NavMeshAgent navAgt;
	NavMeshObstacle navObc;
	BarrierCollider barrierCollider;
	Health guardianHealth;
	Shot guardianShoot;
    EnemyPhyAttack phyAttack;
	bool b_barrier;
	bool isDead;
	bool shooting;
	float curSpeed;
	Vector3 previousPos;


	public bool IsDead{get{return isDead;}set{isDead = value;}}

	public override void Awake () {
		base.Awake ();

		navAgt = GetComponent<NavMeshAgent> ();
		navObc = GetComponent<NavMeshObstacle> ();
		barrierCollider = GetComponentInChildren<BarrierCollider> ();
		guardianHealth = GetComponent<Health> ();
		guardianShoot = GetComponent<Shot> ();
        phyAttack = GetComponent<EnemyPhyAttack>();
		anim = GetComponent<Animator> ();
	}

	public override void Start(){
		base.Start ();
		navAgt.enabled = true;
		navObc.enabled = false;
		initialPoint = transform.position;
        
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update ();
		isDead = guardianHealth.IsDead;
		b_barrier = barrierCollider.Barrier;

		NavManager ();
		Animations ();
	}

	public override void FixedUpdate(){
		base.FixedUpdate ();

	}

    void NavManager()
    {

        Vector3 curMove = transform.position - previousPos;
        curSpeed = curMove.magnitude / Time.deltaTime;
        previousPos = transform.position;

        //-As variaveis a seguir seram as que seram gerenciadas diacordo com os sensores
        //------------------------------------------------------------------------
        //-bool isDead >> Verifica se o agente ta na espera para ser reativado 
        //-bool b_barrier >> Verifica se o agente está no limite de sua area
        //-target == transform >> verifica se tem um inimigo em em sua area
        //-targetToShoot == transform >> verifica se o inimigo está na area de tiro
        //-targetMinDistance == transform >> verifica se o inimigo esta na area minima para se aproximar
        if (!isDead){
            if (!b_barrier) {
                if (!targetSensorArea && !targetToShoot && !targetToAttack )
                {
                    navObc.enabled = false;
                    navAgt.enabled = true;

                    if (Vector3.Distance(this.transform.position, initialPoint) > 1)
                    {
                        ApllyExtraRotation(initialPoint);
                        navAgt.SetDestination(initialPoint);
                    }
                }
                else if (targetSensorArea && !targetToShoot && !targetToAttack)
                {
                    //se ele apenas entrou na area do sensor
                    navObc.enabled = false;
                    navAgt.enabled = true;

                    ApllyExtraRotation(targetSensorArea.position);
                    navAgt.SetDestination(targetSensorArea.position);

                }
                else if (targetSensorArea && targetToShoot && !targetToAttack)
                {
                    //se ele entrou na area do sensor, e na area de tiro
                    navObc.enabled = false;
                    navAgt.enabled = true;

                    ApllyExtraRotation(targetToShoot.position);
                    navAgt.SetDestination(targetToShoot.position);

                    if (guardianShoot && inSight)
                        guardianShoot.ShootEnemy(finalPoint);

                }
                else if (targetSensorArea && targetToAttack)
                {
                    //se ele entrou na distancia minima para aproximação
                    navAgt.enabled = false;
                    navObc.enabled = true;

                    ApllyExtraRotation(targetToAttack.position);

                    //guardianAttack.ShootEnemy (targetToAttack); //<<<=============== SET PHYSICAL ATTACK
                    if (phyAttack && inSight)
                    {
                        phyAttack.AttackEnemy();

                    }
                }
            }
            else
            {
                if (targetSensorArea)
                {
                    //se o inimigo consta no sensor, mas depois do limite de territorio
                    navAgt.enabled = false;
                    navObc.enabled = true;

                    ApllyExtraRotation(targetSensorArea.position);
                    
                    if (guardianShoot && inSight)
                        guardianShoot.ShootEnemy(finalPoint);

                }
                else
                {
                    navObc.enabled = false;
                    navAgt.enabled = true;

                    ApllyExtraRotation(initialPoint);
                    navAgt.SetDestination(initialPoint);
                }
            }
        } 
	}

	void ApllyExtraRotation(Vector3 target){
		Vector3 targetPos = target;
		targetPos.y = transform.position.y;
		Quaternion targetDir = Quaternion.LookRotation (targetPos - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, targetDir, MovingTurnSpeed * Time.fixedDeltaTime);
	}

	void Animations(){
		anim.SetFloat ("Move", curSpeed);
        
	}


}
