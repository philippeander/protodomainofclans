﻿using UnityEngine;
//using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;


public class GuardianManager : MonoBehaviour {

	[Space(10)]
	[Header("GUADIANS CONFIG")]
	[SerializeField]GameObject blueGuardianPrefab;
	[SerializeField]GameObject redGuardianPrefab;
	[SerializeField]Transform[] guardiansSpawnerPoints_Blue_Side = null;
	[SerializeField]Transform[] guardiansSpawnerPoints_Red_Side = null;
	//[SerializeField]int guardVez = 0;

    [Space(10)]
    [Header("BULLETS VARIABLES")]
    [SerializeField]
    protected GameObject bulletPrefab;
    [SerializeField] protected int numOfBulletsByHunter = 6;

    Transform bulletContainerObj;
    BulletManager bulletManager;

    GameManager gameManager;


    void Awake(){
        bulletManager = GameObject.Find("GameManager").GetComponent<BulletManager>();
        bulletContainerObj = GameObject.Find("CharsList/m_Bullets/B_Guardian").transform;
        gameManager = GetComponent<GameManager> ();
        

    }

	// Use this for initialization
	void Start () {

		GuardianInit ();
	}


	void GuardianInit(){

		for(int i = 0; i < guardiansSpawnerPoints_Blue_Side.Length; i++){
			GameObject guardInst = Instantiate (blueGuardianPrefab, guardiansSpawnerPoints_Blue_Side[i].transform.position, guardiansSpawnerPoints_Blue_Side[i].transform.rotation ) as GameObject;
            
            gameManager.SetCharProperties_Blue_Side(guardInst.GetComponent<CharTags>());
            guardInst.transform.SetParent (GameObject.Find("CharsList/Guardians").transform);
			gameManager.ListOfAllChar.Add (guardInst);
            SpownBullets();

            //NetworkServer.Spawn(guardInst);
        }

		for(int i = 0; i < guardiansSpawnerPoints_Red_Side.Length; i++){
			GameObject guardInst = Instantiate (redGuardianPrefab, guardiansSpawnerPoints_Red_Side[i].transform.position, guardiansSpawnerPoints_Red_Side[i].transform.rotation ) as GameObject;
            
            gameManager.SetCharProperties_Red_Side(guardInst.GetComponent<CharTags>());
            guardInst.transform.SetParent (GameObject.Find("CharsList/Guardians").transform);
			gameManager.ListOfAllChar.Add (guardInst);
            SpownBullets();

            //NetworkServer.Spawn(guardInst);
        }
	}

    protected void SpownBullets()
    {
        for (int k = 0; k < numOfBulletsByHunter; k++)
        {
            GameObject bullet_inst = Instantiate(bulletPrefab, transform.position, transform.rotation) as GameObject;

            bullet_inst.transform.parent = bulletContainerObj;
            bullet_inst.transform.position = Vector3.zero;

            bulletManager.GuardianBullets.Add(bullet_inst);

            bullet_inst.SetActive(false);
        }

    }
}
