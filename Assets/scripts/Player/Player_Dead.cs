﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player_Dead : NetworkBehaviour
{

    public Health healthScript;
    Player_UIManager uiManager;
    Player_NetworkSetup plNet_Setup;
    public Vector3 initialPos = Vector3.zero;

    void Awake()
    {
        

    }

    void Start()
    {
        healthScript = GetComponent<Health>();
        uiManager = GetComponent<Player_UIManager>();
        plNet_Setup = GetComponent<Player_NetworkSetup>();

        healthScript.EventDie += DisablePlayer;

        //..
        initialPos = plNet_Setup.StartPos.position;
        
    }

    public override void OnNetworkDestroy()
    {
        healthScript.EventDie -= DisablePlayer;
    }

    void DisablePlayer()
    {
        
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Rigidbody>().isKinematic = true;

        Renderer[] renderes = GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderes.Length; i++) {
            renderes[i].enabled = false;
        }

        healthScript.IsDead = true;
        
        if (healthScript.ReturnToInitialPos)
            transform.position = initialPos;

        if (isLocalPlayer)
        {
            uiManager.Img_CrossHair.enabled = false;
            GetComponent<PlayerControllers>().enabled = false;
        }

        //healthScript.RpcIfUpdateEnemyValues();
        healthScript.RegenetareAndUpdateValues();

        healthScript.ShouldDie = false;

    }
}
