﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_UIManager : MonoBehaviour {

    public Sprite avatarIcon; 

    Text txtActiveCount;

    Slider sld_Life;
    Slider sld_Energy;

    //Skill 1 variables
    Skills skills;

    GameObject uiSkill_1;
    Slider sld_Time_1;
    Text txt_actions_1;
    Text txt_CountDown_1;

    //Skill 1 variables
    GameObject uiSkill_2;
    Slider sld_Time_2;
    Text txt_actions_2;
    Text txt_CountDown_2;

    //Ult 1 variables
    GameObject uiUlt;
    Slider sld_Time_ult;
    Text txt_actions_ult;
    Text txt_CountDown_ult;

    Health playerHealth;

    Image img_CrossHair;

    int playerLevelHist;

    public Image Img_CrossHair
    {
        get
        {
            return img_CrossHair;
        }

        set
        {
            img_CrossHair = value;
        }
    }
    public Text TxtActiveCount
    {
        get
        {
            return txtActiveCount;
        }

        set
        {
            txtActiveCount = value;
        }
    }

    private void Awake()
    {
        txtActiveCount = GameObject.Find("Canvas/TextActiveCount").GetComponent<Text>();

        
    }

    void Start()
    {
        GetUiElementsToPlayer_Start();
        playerHealth = GetComponent<Health>();

        img_CrossHair = GameObject.Find("Canvas/Crosshair").GetComponent<Image>();

        UiManager_Start();

        txtActiveCount.enabled = false;

        GameObject.Find("Canvas/Player Image/Modure_FaceCamera").GetComponent<Image>().sprite = avatarIcon;
    }

    private void Update()
    {
        if(playerHealth.PlayerLevel > playerLevelHist)
            playerLevelHist = playerHealth.PlayerLevel;

        UiManager_Update();
    }

    void GetUiElementsToPlayer_Start()
    {
        //Responsavel por pegar todos objetos UI a serem usados para o player.

        //Skill 1
        uiSkill_1 = GameObject.FindGameObjectWithTag("Skill_1"); //By TAG, RELACIONADO AO ICONE NA UI
        sld_Time_1 = uiSkill_1.GetComponentInChildren<Slider>(); //RELACIONADO AO ICONE NA UI
        txt_actions_1 = GameObject.Find("BtnSkill_1/txtButtonToAction").GetComponent<Text>();//RELACIONADO AO ICONE NA UI
        txt_CountDown_1 = GameObject.Find("BtnSkill_1/txtCountDown").GetComponent<Text>();//RELACIONADO AO ICONE NA UI

        //Skill 2
        uiSkill_2 = GameObject.FindGameObjectWithTag("Skill_2"); //By TAG, RELACIONADO AO ICONE NA UI
        sld_Time_2 = uiSkill_2.GetComponentInChildren<Slider>(); //RELACIONADO AO ICONE NA UI
        txt_actions_2 = GameObject.Find("BtnSkill_2/txtButtonToAction").GetComponent<Text>();//RELACIONADO AO ICONE NA UI
        txt_CountDown_2 = GameObject.Find("BtnSkill_2/txtCountDown").GetComponent<Text>();//RELACIONADO AO ICONE NA UI

        //Ult
        uiUlt = GameObject.FindGameObjectWithTag("Ult");
        sld_Time_ult = uiUlt.GetComponentInChildren<Slider>();
        txt_actions_ult = GameObject.Find("BtnUlt/txtButtonToAction").GetComponent<Text>();
        txt_CountDown_ult = GameObject.Find("BtnUlt/txtCountDown").GetComponent<Text>();

        //Others
        sld_Life = GameObject.Find("Sld_PlayerLife").GetComponent<Slider>();
        sld_Energy = GameObject.Find("Sld_PlayerEnergy").GetComponent<Slider>();

    }

    void UiManager_Start()
    {

        skills = GetComponent<Skills>();

        sld_Time_1.maxValue = skills.Skill_1_Paramiters.timeNum;
        sld_Time_2.maxValue = skills.Skill_2_Paramiters.timeNum;
        sld_Time_ult.maxValue = skills.Ult_Paramiters.timeNum;

        sld_Life.maxValue = playerHealth.Life_MaxValue;
        sld_Energy.maxValue = playerHealth.Energy_MaxValue;

    }

    void UiManager_Update()
    {
        //Verifica o nivel do player para igualar o MaxValue dos slides com o MaxValue das Variaveis
        if (playerHealth.PlayerLevel > playerLevelHist)
        {
            sld_Life.maxValue = playerHealth.Life_MaxValue;
            sld_Energy.maxValue = playerHealth.Energy_MaxValue;
            playerLevelHist = playerHealth.PlayerLevel;
        }

        sld_Life.value = playerHealth.LifeCurrent;
        sld_Energy.value = playerHealth.EnergyCurrent;

        //Pega o valor do CountDown e joga no RadialSlide do botão
        sld_Time_1.value = skills.Skill_1_Paramiters.countLibSkill;
        sld_Time_2.value = skills.Skill_2_Paramiters.countLibSkill;
        sld_Time_ult.value = skills.Ult_Paramiters.countLibSkill;

        //Se o countDown for maior que 0
        //Se nivel do jogador for maior que "N"
        //Se a skill não estiver ativa
        // = A countDown Ativo 
        if (skills.CountDown(skills.Skill_1_Paramiters))
        {
            txt_CountDown_1.text = skills.Skill_1_Paramiters.countLibSkill.ToString("0");
            txt_actions_1.gameObject.SetActive(false);
        }
        else
        {
            txt_CountDown_1.text = "";
            txt_actions_1.gameObject.SetActive(true);
        }

        //---------------------------------------------------------

        if (skills.CountDown(skills.Skill_2_Paramiters))
        {
            txt_CountDown_2.text = skills.Skill_2_Paramiters.countLibSkill.ToString("0");
            txt_actions_2.gameObject.SetActive(false);
        }
        else
        {
            txt_CountDown_2.text = "";
            txt_actions_2.gameObject.SetActive(true);
        }

        //-------------------------------------------------------------

        //Player skill 1 CountDown()
        if (skills.CountDown(skills.Ult_Paramiters))
        {
            txt_CountDown_ult.text = skills.Ult_Paramiters.countLibSkill.ToString("0");
            txt_actions_ult.gameObject.SetActive(false);
        }
        else
        {
            txt_CountDown_ult.text = "";
            txt_actions_ult.gameObject.SetActive(true);
        }

        //-----------------------------------------------------------

    }
}
