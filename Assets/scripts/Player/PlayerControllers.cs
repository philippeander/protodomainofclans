using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerMotor))]
public class PlayerControllers : NetworkBehaviour {

	PlayerMotor m_Character;
	public Vector3 m_Move = Vector3.zero;
	bool m_Jump = false;
	//Skills Input
	bool slc_Skill_1;
	bool slc_Skill_2;
	bool slc_Ult;
	bool attackSkill;
	bool trapsAndTricks;
	bool attackPassiva;
	Skills skills;
	Player_Shoot playerPassiva;
	PhysicalAttack phyAttack;
    //PlayerProperties plProps;

    public Vector2 mouseMoviment;

    public Vector2 MouseMoviment
    {
        get
        {
            return mouseMoviment;
        }

        set
        {
            mouseMoviment = value;
        }
    }

    void Awake()
    {
		m_Character = GetComponent<PlayerMotor> ();
		skills = GetComponent<Skills> ();
        //plProps = GetComponent<PlayerProperties>();

		if(GetComponent<Player_Shoot> ())
			playerPassiva = GetComponent<Player_Shoot> ();

		if (GetComponent<PhysicalAttack> ())
			phyAttack = GetComponent<PhysicalAttack> ();
	}
    
    void Update(){

        if (!isLocalPlayer) return;

        mouseMoviment = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        if (!m_Jump){m_Jump = CrossPlatformInputManager.GetButtonDown ("Jump");}

        SkillsManager ();
        
    }

    private void FixedUpdate(){

        if (!isLocalPlayer) return;

        float h = CrossPlatformInputManager.GetAxis ("Horizontal");
		float v = CrossPlatformInputManager.GetAxis ("Vertical");


		m_Move = v * Vector3.forward + h * Vector3.right;
		

		if(Input.GetKey(KeyCode.LeftShift)){m_Move *= 0.5f;}

		m_Character.Move (m_Move, m_Jump);

		m_Jump = false;
	}

	void SkillsManager(){

		if (!slc_Skill_1) slc_Skill_1 = Input.GetKeyUp (KeyCode.Alpha1);
		if (!slc_Skill_2) slc_Skill_2 = Input.GetKeyUp (KeyCode.Alpha2);
		if (!slc_Ult) slc_Ult = Input.GetKeyUp (KeyCode.Alpha3);
		if (!attackSkill) attackSkill = Input.GetKeyUp (KeyCode.Mouse1);
		if (!trapsAndTricks) trapsAndTricks = Input.GetKeyUp (KeyCode.Q);

		if (!attackPassiva) attackPassiva = Input.GetKeyDown (KeyCode.Mouse0);

		if(GetComponent<Skills>())
			skills.CmdGetInput (slc_Skill_1, slc_Skill_2, slc_Ult, trapsAndTricks, attackSkill, attackPassiva);


        if (attackPassiva && playerPassiva)
        {
            
            playerPassiva.ShootEnemy();
        }
		if (attackPassiva && phyAttack)
			phyAttack.AttackEnemy ();


		slc_Skill_1 = false;
		slc_Skill_2 = false;
		slc_Ult = false;
		attackSkill = false;
		trapsAndTricks = false;
		attackPassiva = false;
	}

}
