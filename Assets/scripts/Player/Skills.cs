﻿/*
Numeros do teclado para Habilitar a Skill
E btn direito do mouse para usar
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class Skills : NetworkBehaviour {

	enum PlayerName{Tania, Nicolay}

	[Serializable]public class SkillParamiters
	{
		[Space(10)]
		public GameObject[] objsToShow;			//Objetos que só serão ativos quando forem usados 

		[Space(10)]
		public Transform[] spownerPoints;		//para spownar objetos

		[Space(10)]
		public GameObject bulletPrefab;		

		[Space(10)]
		public bool animationEvent = false;			//True = EnterAnimationEvent; False = ExitAnimationEvent
		public float timeNum = 15;				//Tempo de espera após usar a habilidade
		public float minLevelRequired = 2;		//Level minnimo do player para usar a Habilidade
		public float attackDelay = 1f;			//Tempo de espera para os objetos da skill ser desativado

		[Space(10)]
		public bool blockSkill;        //Para Bloquear a habilidade quando não atender os requitos
        public bool activeSkill;  //Ativa a Habilidade antes de ser usada
        public bool usingSkill;		//Para certificar que apenas uma Habilidade seja usada por vez

		[Space(10)]
		public float countLibSkill;		//Contador de tempo para liberar a skill
        
    }


	[SerializeField]PlayerName playerName; // Vai servir para setar o polimorfismo

	[Space(10)]
	[SerializeField]SkillParamiters skill_1_Paramiters;
	[SerializeField]SkillParamiters skill_2_Paramiters;
	[SerializeField]SkillParamiters ult_Paramiters;

    bool init = false;

	public int playerLevel = 0;
	public Vector3 point;
	//Aqui ele vai guardar os scriipts referentes a cada personagem
	ISkillStates skill_1;
	ISkillStates skill_2;
	ISkillStates ult;
    
    SkillParamiters[] skillsArray = new SkillParamiters[3]; //(0)=skill 1 :(1)=skill 2 : (2)=Ult
	ISkillStates[] ISkillStates = new ISkillStates[3];

    //PlayerProperties plProps;

	public SkillParamiters Skill_1_Paramiters{get{return skill_1_Paramiters;}}
	public SkillParamiters Skill_2_Paramiters{get{return skill_2_Paramiters;}}
	public SkillParamiters Ult_Paramiters{get{return ult_Paramiters;}}
	//Health health;

	void Awake(){
        //health = GetComponent<Health> ();
        //plProps = GetComponent<PlayerProperties>();
	}

	void Start () {
        Init();

        
	}

    void Init() {

        if (!init)
        {
            //Pega o script de acordo com o personagem e joga para os "states Machine"
            if (playerName == PlayerName.Tania)
            {
                skill_1 = gameObject.AddComponent<TaniaSkill_1>(); ;
                skill_2 = gameObject.AddComponent<TaniaSkill_2>();
                ult = gameObject.AddComponent<TaniaUlt>();
            }
            else if (playerName == PlayerName.Nicolay)
            {
                skill_1 = gameObject.AddComponent<NikolaiSkill_1>();
                skill_2 = gameObject.AddComponent<NikolaiSkill_2>();
                ult = gameObject.AddComponent<NikolaiUlt>();
            }

            /*
            "For()" abaixo seta as configurações iniciais de cada Habilidade
            Lembrando que cada elemento do Array "skillsArray" corresponde a:
            */
            skillsArray[0] = skill_1_Paramiters;
            skillsArray[1] = skill_2_Paramiters;
            skillsArray[2] = ult_Paramiters;

            ISkillStates[0] = skill_1;
            ISkillStates[1] = skill_2;
            ISkillStates[2] = ult;

            //Seta as propriedades da Interface ISkillStates
            for (int i = 0; i < ISkillStates.Length; i++)
            {
                ISkillStates[i].Obj = skillsArray[i].objsToShow;
                ISkillStates[i].SpownerPoints = skillsArray[i].spownerPoints;
                if (GetComponent<Handler>())
                {
                    ISkillStates[i].IkHandler = GetComponent<Handler>().ShoulderRotation;
                }
                ISkillStates[i].BulletPrefab = skillsArray[i].bulletPrefab;
                ISkillStates[i].thisGameObject = this.gameObject;

                ISkillStates[i].Init();
            }

            //Inicia a variaveis dos Objetos SkillParamiters
            for (int i = 0; i < skillsArray.Length; i++)
            {

                if (skillsArray[i].objsToShow.Length > 0)
                {

                    for (int j = 0; j < skillsArray[i].objsToShow.Length; j++)
                    {
                        skillsArray[i].objsToShow[j].SetActive(false);
                    }
                }
                skillsArray[i].usingSkill = false;
                skillsArray[i].blockSkill = false;
                skillsArray[i].activeSkill = false;
                skillsArray[i].countLibSkill = skillsArray[i].timeNum;

                //print("GG");
                init = true;
            }
        }
            
    }

    //Seta a Habilidade de acordo com o Input
    //Para usar a skill teem que passar por três estagios
    //blockSkill    - Compara o nivel que o jogador está para liberar a skill
    //activeSkill   - Destrava a skill para ser utilizada
    //useSkill      - Despara o atacck da skill

    [Command]
	public void CmdGetInput(bool sk_1, bool sk_2, bool ult, bool t_t, bool attackSkill, bool passiva){
        
        RpcExecultSkills(sk_1, sk_2, ult, t_t, attackSkill, passiva);
	}

    [ClientRpc]
    void RpcExecultSkills(bool sk_1, bool sk_2, bool ult, bool t_t, bool attackSkill, bool passiva) {

        Init();

        //Pega o ponto final do Raycast posicionado no centro da Camera
        point = GetComponent<Player_NetworkSetup>().FinalPointTarget.position;

        //Seta a propriedade PointTarget
        //E libera possiveis Eventos de Animação
        for (int i = 0; i < ISkillStates.Length; i++)
        {
            ISkillStates[i].PointTarget = point;
            if (skill_1_Paramiters.animationEvent || skill_2_Paramiters.animationEvent || ult_Paramiters.animationEvent)
            {
                ISkillStates[i].AnimationEvent = true;
            }
            else
            {
                ISkillStates[i].AnimationEvent = false;
            }
        }


        for (int i = 0; i < skillsArray.Length; i++)
        {
            //Libera a Habilidade de acordo com o Nivel do Personagem
            if (playerLevel > skillsArray[i].minLevelRequired)
            {
                skillsArray[i].blockSkill = true;
            }
            //Se a habilidade estiver Distravada ele Faz o Countdown para stivar a Habilidade
            if (skillsArray[i].blockSkill && !skillsArray[i].activeSkill)
            {
                skillsArray[i].countLibSkill -= Time.deltaTime;
                if (skillsArray[i].countLibSkill <= 0)
                {
                    skillsArray[i].activeSkill = true;
                }
            }
        }

        //Abaixo eles comparam as variaveis para liberar o uso  da Skill
        if (sk_1 && skill_1_Paramiters.activeSkill)
        {
            skill_1_Paramiters.usingSkill = true;
        }
        else if (sk_2 || ult || t_t || passiva)
        {
            skill_1_Paramiters.usingSkill = false;
        }
        if (sk_2 && skill_2_Paramiters.activeSkill)
        {
            skill_2_Paramiters.usingSkill = true;
        }
        else if (sk_1 || ult || t_t || passiva)
        {
            skill_2_Paramiters.usingSkill = false;
        }
        if (ult && ult_Paramiters.activeSkill)
        {
            ult_Paramiters.usingSkill = true;
        }
        else if (sk_1 || sk_2 || t_t || passiva)
        {
            ult_Paramiters.usingSkill = false;
        }

        //Chama as skill
        skill_1.ExecultSkillAttack(skill_1_Paramiters.usingSkill, attackSkill);

        skill_2.ExecultSkillAttack(skill_2_Paramiters.usingSkill, attackSkill);

        this.ult.ExecultSkillAttack(ult_Paramiters.usingSkill, attackSkill);

        //Delay para finalizar o ataque da skill 
        for (int i = 0; i < skillsArray.Length; i++)
        {
            if (skillsArray[i].usingSkill && attackSkill)
            {
                StartCoroutine(DeactiveDelay(skillsArray[i]));
            }
        }
    }

    //Consulta a situação do CountDown de tal SkillParamiters Object 
    public bool CountDown(SkillParamiters countValue){
		if (countValue.countLibSkill >= 0 && countValue.blockSkill && !countValue.activeSkill) {
			return true;
		} else {
			return false;
		}
	}


	IEnumerator DeactiveDelay(SkillParamiters obj){
		
		yield return new WaitForSeconds (obj.attackDelay);

		for (int i = 0; i < obj.objsToShow.Length; i++) {
			obj.objsToShow [i].SetActive (false);
		}

		FinalizeAttack (obj);

	}

	void FinalizeAttack(SkillParamiters obj){
		print ("Kame-Hame-Haaaaa");
		obj.countLibSkill = obj.timeNum;
		obj.activeSkill = false;
		obj.usingSkill = false;
	}



	//--------------------------------------------------

	void EnterAnimEvent_skill_1(){
		skill_1_Paramiters.animationEvent = true;
	}

	void EnterAnimEvent_skill_2(){
		skill_2_Paramiters.animationEvent = true;
	}

	void EnterAnimEvent_ult(){
		ult_Paramiters.animationEvent = true;
	}

	//----------------------------------------------------

	void ExitAnimEvent_skill_1(){
		skill_1_Paramiters.animationEvent = false;
	}

	void ExitAnimEvent_skill_2(){
		skill_2_Paramiters.animationEvent = false;
	}

	void ExitAnimEvent_ult(){
		ult_Paramiters.animationEvent = false;
	}

}
