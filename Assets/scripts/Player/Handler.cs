﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handler : MonoBehaviour {
    
	//[SerializeField]int layer_Index = 0; 

	[Space(10)]
	[SerializeField]float lerpRate = 15;
	[SerializeField]float updateLookPosThreshold = 2;

	[Space(10)]
	[SerializeField]float lookWeight = 1;
	[SerializeField]float bodyWeight = .9f;
	[SerializeField]float headWeigth = 1;
	[SerializeField]float clampWeight = 1;

	[Space(10)]
	[SerializeField]float rightHandWeight = 1;
	[SerializeField]float leftHandWeigth = 1;

	[Space(10)]
	[SerializeField]Transform rightHandTarget;
	[SerializeField]Transform rightElbowTarget;
	[SerializeField]Transform leftHandTarget;
	[SerializeField]Transform leftElbowTarget;

	[Space(10)]
	[SerializeField]Transform shoulderTrans;
	[SerializeField]Transform rightShouder;

	[Space(10)]
	//[SerializeField]Transform target;
    [SerializeField]GameObject rsp;


    Animator anim;
	Vector3 lookPos;
    Vector3 IK_lookPos;
    Vector3 targetPos;

    Player_NetworkSetup player_network;

	//public Transform Target{get{return target;}set{target = value;}}
	public Transform ShoulderRotation{get{return shoulderTrans;}}

    //public GameObject _rightHand;

    void Awake()
    {
		anim = GetComponent<Animator> ();   
		//_rightHand = anim.GetBoneTransform (HumanBodyBones.RightHand).gameObject;
        
	}

    private void Start()
    {
        player_network = GetComponent<Player_NetworkSetup>();
    }


    void FixedUpdate(){
        

		HandleShouder ();
	}


	void OnAnimatorIK(){

        
        anim.SetIKPositionWeight(AvatarIKGoal.RightHand, rightHandWeight);              //Position Weight of Right Hand
        anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, leftHandWeigth);                //Position Weight of Left Hand

        anim.SetIKPosition(AvatarIKGoal.RightHand, rightHandTarget.position);           //Position of Right Hand
        anim.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTarget.position);         //Position of Left Hand

        anim.SetIKRotationWeight(AvatarIKGoal.RightHand, rightHandWeight);              //Rotation Weight of Right Hand
        anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, leftHandWeigth);                //Rotation Weight of Left Hand

        anim.SetIKRotation(AvatarIKGoal.RightHand, rightHandTarget.rotation);           //Rotation of Right Hand
        anim.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTarget.rotation);         //Rotation of Left Hand

        anim.SetIKHintPositionWeight(AvatarIKHint.RightElbow, rightHandWeight);     //Position Weight of Right Elbow
        anim.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, leftHandWeigth);           //Position Weight of Left Elbow

        anim.SetIKHintPosition(AvatarIKHint.RightElbow, rightElbowTarget.position); //Position of Right Elbow
        anim.SetIKHintPosition(AvatarIKHint.LeftElbow, leftElbowTarget.position);       //Position of Left Elbow

        lookPos = player_network.FinalPointTarget.position;
        //lookPos.y = transform.position.y;
        //lookPos.y = 0;
        
        float distanceFromPlayer = Vector3.Distance(lookPos, transform.position);

        if (distanceFromPlayer > updateLookPosThreshold)
        {
            targetPos = lookPos;
        }

        IK_lookPos = Vector3.Lerp(IK_lookPos, targetPos, lerpRate * Time.deltaTime);

        anim.SetLookAtWeight(lookWeight, bodyWeight, headWeigth, headWeigth, clampWeight);
        anim.SetLookAtPosition(IK_lookPos);
        
    }

	void HandleShouder(){

		shoulderTrans.LookAt (lookPos);

		Vector3 rightShouderPos = rightShouder.TransformPoint (Vector3.zero);
		rsp.transform.position = rightShouderPos;
		rsp.transform.parent = transform;
		shoulderTrans.position = rsp.transform.position;

	}
}
