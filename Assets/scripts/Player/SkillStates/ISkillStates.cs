﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkillStates {

	GameObject[] Obj{ get; set;}
	Transform[] SpownerPoints{ get; set;}

	GameObject thisGameObject{ get; set;}
	GameObject BulletPrefab{get; set;}
	Transform IkHandler{ get; set;}
	Vector3 PointTarget{get; set;}
	bool AnimationEvent{get; set;}

	void Init();

	void ExecultSkillAttack (bool useSkill, bool attackSkill);

}
