﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;


public class TaniaSkill_2 : NetworkBehaviour, ISkillStates {

	GameObject[] obj;
	Transform[] spownerPoints;

	GameObject bulletPrefab;
	Transform ikHandler;
	Vector3 pointTarget;

	public GameObject[] Obj{ get{return obj;} set{obj = value;}}
	public Transform[] SpownerPoints{ get{return spownerPoints;} set{spownerPoints = value;}}

	public GameObject thisGameObject{ get; set;}
	public GameObject BulletPrefab{ get{return bulletPrefab;} set{bulletPrefab = value;}}
	public Transform IkHandler{ get{return ikHandler;} set{ikHandler = value;}}
	public Vector3 PointTarget{ get{return pointTarget;} set{pointTarget = value;}}
	public bool AnimationEvent{ get; set;}

	public void Init (){

	}

    //[Command]
    public void ExecultSkillAttack (bool useSkill, bool attackSkill){

		ParticleSystem[] m_particle;
		m_particle = obj [1].GetComponentsInChildren<ParticleSystem> ();

		
		if (useSkill) {
			obj [0].SetActive (true);
            obj[1].SetActive(true);
            obj [0].transform.position = new Vector3 (pointTarget.x, 0.3f, pointTarget.z);

			Vector3 pos = obj [0].transform.position - ikHandler.position;
			pos.y = 0f;
            obj [0].transform.rotation = Quaternion.LookRotation (pos);

		} else {
			obj [0].SetActive (false);
		}
		

		if (attackSkill && useSkill) {
            
            foreach (ParticleSystem PS in m_particle){
				PS.Play ();
			}

            
            obj[2].SetActive(true);
        }
	}
 
}
