﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

public class TaniaSkill_1 : NetworkBehaviour, ISkillStates {

	GameObject[] obj;
	Transform[] spownerPoints;

	GameObject bulletPrefab;
	Transform ikHandler;
	Vector3 pointTarget;


	public GameObject[] Obj{ get{return obj;} set{obj = value;}}
	public Transform[] SpownerPoints{ get{return spownerPoints;} set{spownerPoints = value;}}

	public GameObject thisGameObject{ get; set;}
	public GameObject BulletPrefab{ get{return bulletPrefab;} set{bulletPrefab = value;}}
	public Transform IkHandler{ get{return ikHandler;} set{ikHandler = value;}}
	public Vector3 PointTarget{ get{return pointTarget;} set{pointTarget = value;}}
	public bool AnimationEvent{ get; set;}

	public void Init (){

	}

    //[Command] //VERIFICAR COMO FICA ESSE COMMAND
	public void ExecultSkillAttack (bool useSkill, bool attackSkill){

		
		if (useSkill) {
			obj [0].SetActive (true);

			Vector3 pos = pointTarget - ikHandler.position;
			pos.y = 0f;
			obj [0].transform.rotation = Quaternion.LookRotation (pos);

		} else {
			obj [0].SetActive (false);
		}
		
		
		if (attackSkill && useSkill) {

            CharTags tegs = GetComponentInParent<CharTags>();
            
            for (int i = 0; i < spownerPoints.Length; i++)
            {
                GameObject instBullet_1 = Instantiate(bulletPrefab, spownerPoints[i].position, spownerPoints[i].rotation) as GameObject;
                instBullet_1.GetComponent<Rigidbody>().velocity = spownerPoints[i].forward * 30.0f;
                instBullet_1.transform.tag = tegs.MyBulletTag;

                BulletLife_Spawn BL = instBullet_1.GetComponent<BulletLife_Spawn>();

                BL.ObjQueDisparou = tegs.gameObject;
                BL.EnemyTag = tegs.EnemyTag;
                BL.BulletValue = 25f;

                //NetworkServer.Spawn(instBullet_1);
                Destroy(instBullet_1, 0.5f);
            }
        }
	}
}
