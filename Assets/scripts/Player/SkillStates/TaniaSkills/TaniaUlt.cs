﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;


public class TaniaUlt : NetworkBehaviour, ISkillStates
{

    GameObject[] obj;
    Transform[] spownerPoints;

    GameObject bulletPrefab;
    Transform ikHandler;
    Vector3 pointTarget;

    public GameObject[] Obj { get { return obj; } set { obj = value; } }
    public Transform[] SpownerPoints { get { return spownerPoints; } set { spownerPoints = value; } }

    public GameObject thisGameObject { get; set; }
    public GameObject BulletPrefab { get { return bulletPrefab; } set { bulletPrefab = value; } }
    public Transform IkHandler { get { return ikHandler; } set { ikHandler = value; } }
    public Vector3 PointTarget { get { return pointTarget; } set { pointTarget = value; } }
    public bool AnimationEvent { get; set; }

    public void Init()
    {

    }

    //[Command]
    public void ExecultSkillAttack(bool useSkill, bool attackSkill)
    {



        for (int i = 0; i < obj.Length; i++)
        {
            if (useSkill)
            {
                obj[i].SetActive(true);
                obj[i].transform.position = new Vector3(pointTarget.x, 0.3f, pointTarget.z);

                Vector3 pos = obj[i].transform.position - ikHandler.position;
                pos.y = 0f;
                obj[i].transform.rotation = Quaternion.LookRotation(pos);

            }
            else
            {
                obj[i].SetActive(false);
            }
        }

        if (attackSkill && useSkill)
        {

            CharTags tegs = GetComponentInParent<CharTags>();


            for (int i = 0; i < spownerPoints.Length; i++)
            {
                GameObject instBullet = Instantiate(bulletPrefab, spownerPoints[i].position, spownerPoints[i].rotation) as GameObject;
                instBullet.GetComponent<Rigidbody>().velocity = spownerPoints[i].forward * 30f;
                instBullet.transform.tag = tegs.MyBulletTag;

                BulletLife_Spawn BL = instBullet.GetComponent<BulletLife_Spawn>();

                BL.ObjQueDisparou = tegs.gameObject;
                BL.EnemyTag = tegs.EnemyTag;
                BL.BulletValue = 25f;

                //NetworkServer.Spawn(instBullet);
                Destroy(instBullet, 0.08f);
            }
        }
    }
    
}
