﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NikolaiSkill_2 : MonoBehaviour, ISkillStates {

	GameObject[] obj;
	Transform[] spownerPoints;

	GameObject bulletPrefab;
	Transform ikHandler;
	Vector3 pointTarget;

    public GameObject[] Obj{ get{return obj;} set{obj = value;}}
	public Transform[] SpownerPoints{ get{return spownerPoints;} set{spownerPoints = value;}}

	public GameObject thisGameObject{ get; set;}
	public GameObject BulletPrefab{ get{return bulletPrefab;} set{bulletPrefab = value;}}
	public Transform IkHandler{ get{return ikHandler;} set{ikHandler = value;}}
	public Vector3 PointTarget{ get{return pointTarget;} set{pointTarget = value;}}
	public bool AnimationEvent{ get; set;}

    public void Init (){

	}

	public void ExecultSkillAttack (bool useSkill, bool attackSkill){

		for(int i = 0; i < obj.Length; i++){
			if (useSkill) {
				obj [i].SetActive (true);
				obj [i].transform.position = new Vector3 (pointTarget.x, 0.0f, pointTarget.z);

			} else {
				obj [i].SetActive (false);
			}

		}

		if (attackSkill && useSkill) {

			//Pega um personagem que esta no circulo e restaura vida e energia


		}

	}
}
