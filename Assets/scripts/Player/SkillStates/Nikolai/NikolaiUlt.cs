﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NikolaiUlt : MonoBehaviour, ISkillStates {

	GameObject[] obj;
	Transform[] spownerPoints;

	GameObject bulletPrefab;
	Transform ikHandler;
	Vector3 pointTarget;

    public GameObject[] Obj{ get{return obj;} set{obj = value;}}
	public Transform[] SpownerPoints{ get{return spownerPoints;} set{spownerPoints = value;}}

	public GameObject thisGameObject{ get; set;}
	public GameObject BulletPrefab{ get{return bulletPrefab;} set{bulletPrefab = value;}}
	public Transform IkHandler{ get{return ikHandler;} set{ikHandler = value;}}
	public Vector3 PointTarget{ get{return pointTarget;} set{pointTarget = value;}}
	public bool AnimationEvent{ get; set;}

    Animator animChar;
	float countDown;
	bool animTime;

	public void Init (){
		animChar = thisGameObject.GetComponent<Animator> ();
		countDown = 5f;
	}

	public void ExecultSkillAttack (bool useSkill, bool attackSkill){

		if (useSkill) {


			if (attackSkill) {
				animTime = true;
			}
		}

		if(animTime){
			animChar.SetBool ("Ult", true);
			countDown -= Time.deltaTime;
			if(countDown <= 0){
				animChar.SetBool ("Ult", false);
				animTime = false;
				countDown = 5f;
			}
		}

	}


}
