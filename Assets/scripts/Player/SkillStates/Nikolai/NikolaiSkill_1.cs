﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NikolaiSkill_1 : MonoBehaviour, ISkillStates {

	GameObject[] obj;
	Transform[] spownerPoints;

	GameObject bulletPrefab;
	Transform ikHandler;
	Vector3 pointTarget;

    public GameObject[] Obj{ get{return obj;} set{obj = value;}}
	public Transform[] SpownerPoints{ get{return spownerPoints;} set{spownerPoints = value;}}

	public GameObject thisGameObject{ get; set;}
	public GameObject BulletPrefab{ get{return bulletPrefab;} set{bulletPrefab = value;}}
	public Transform IkHandler{ get{return ikHandler;} set{ikHandler = value;}}
	public Vector3 PointTarget{ get{return pointTarget;} set{pointTarget = value;}}
	public bool AnimationEvent{ get; set;}

    Animator animLaser;
	Animator animChar;

	public void Init (){
		animLaser = obj [0].GetComponent<Animator> ();
		animChar = thisGameObject.GetComponent<Animator> ();
	}

	public void ExecultSkillAttack (bool useSkill, bool attackSkill){

		if (useSkill) {
			

			if (attackSkill) {
				//Pega um personagem que esta no circulo e restaura vida e energia
				animChar.SetBool ("Skill_1", true);

			} else {
				animChar.SetBool ("Skill_1", false);
			}

			obj [0].SetActive (AnimationEvent);
			animLaser.SetBool ("Fire", AnimationEvent);
		}

	}

}
