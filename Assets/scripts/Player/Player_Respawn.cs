﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Player_Respawn : NetworkBehaviour
{

    private Health healthScript;
    Player_UIManager uiManager;

    void Start()
    {
        healthScript = GetComponent<Health>();
        healthScript.EventRespawn += EnablePlayer;
        uiManager = GetComponent<Player_UIManager>();
    }
    

    public override void OnNetworkDestroy()
    {
        healthScript.EventRespawn -= EnablePlayer;
    }


    void EnablePlayer()
    {
        if (healthScript.IsDead)
        {
            uiManager.TxtActiveCount.enabled = true;
            healthScript.ResurrectCountDown -= Time.deltaTime;

            uiManager.TxtActiveCount.text = string.Format("{0:0}:{1:00}", Mathf.Floor(healthScript.ResurrectCountDown / 60), healthScript.ResurrectCountDown % 60) + " / " +
                string.Format("{0:0}:{1:00}", Mathf.Floor(healthScript.ResurrectMaxValue / 60), healthScript.ResurrectMaxValue % 60) + "\nto Ressurrect";

            if (healthScript.ResurrectCountDown <= 0)
            {
                uiManager.TxtActiveCount.enabled = false;

                
                GetComponent<CapsuleCollider>().enabled = true;
                GetComponent<Rigidbody>().isKinematic = false;

                Renderer[] renderes = GetComponentsInChildren<Renderer>();
                for (int i = 0; i < renderes.Length; i++)
                {
                    renderes[i].enabled = true;
                }

                if (isLocalPlayer)
                {
                    uiManager.Img_CrossHair.enabled = true;
                    GetComponent<PlayerControllers>().enabled = true;

                }

                healthScript.IsDead = false;
                uiManager.TxtActiveCount.text = "";
            }
        }
    }
}
