﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_ID : NetworkBehaviour {

    [SyncVar]public string playerUniqueIdentity = "Unamed";

    private void Start()
    {
        //Debug.Log("TEST ID: " + connectionToClient.connectionId);
    }

    public void SetStringName(string name)
    {
        CmdRenamed(name);
    }

    [Command]
    void CmdRenamed(string name)
    {
        playerUniqueIdentity = name;
    }

    
}
