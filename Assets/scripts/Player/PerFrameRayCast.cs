﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PerFrameRayCast : NetworkBehaviour {

    [SerializeField] Transform finalPointObj;
    [SerializeField]Transform objPoint;
    [SerializeField] Transform rayOrigin;
	[SerializeField]float rayCastMaxDistance = 20f;
	[SerializeField]float raySizePercentage = 100f;
	[SerializeField]bool debugRay = false;
    
    RaycastHit rayCast;
    Vector3 finalPoint;
    float rayDistance;

    //public float RayCastMaxDistance{get{return rayDistance;}set{rayDistance = value;}}
	//public Transform ObjPoint{get{return objPoint;} set{objPoint = value;}}

    // Use this for initialization
    void Awake()
    {
        rayDistance = rayCastMaxDistance * (raySizePercentage / 100);
	}

    private void Start()
    {
        finalPointObj = GetComponent<Player_NetworkSetup>().FinalPointTarget;
    }

    // Update is called once per frame
    void FixedUpdate () {

        if (!isLocalPlayer) return;

        PerFrameRaycast();
        
	}

    
	void PerFrameRaycast(){
        
		Ray ray = new Ray (rayOrigin.transform.position, objPoint.transform.forward);
		RaycastHit hitInfo;

		Physics.Raycast (ray, out hitInfo, rayDistance);
		rayCast = hitInfo;

		if (rayCast.transform) {
			finalPoint = rayCast.point;
		} else {
			finalPoint = rayOrigin.transform.position + (objPoint.transform.forward * rayCastMaxDistance);
		}

		if(debugRay){
			//Debug.DrawRay (objPoint.transform.position, finalPoint, Color.red);
			Debug.DrawRay (rayOrigin.position, objPoint.transform.forward * rayDistance, Color.cyan );

		}

        finalPointObj.position = finalPoint;
	}
    
}
