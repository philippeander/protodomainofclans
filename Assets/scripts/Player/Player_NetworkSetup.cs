﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_NetworkSetup : NetworkBehaviour
{
    [SerializeField] Camera TPSCharacterCam;
    [SerializeField] AudioListener audioListener;

    PlayerProperties plProps;
    GameManager gameManager;
    Transform finalPointTarget;
    Transform startPos;

    public Transform FinalPointTarget
    {
        get
        {
            return finalPointTarget;
        }

        set
        {
            finalPointTarget = value;
        }
    }
    public Transform StartPos
    {
        get
        {
            return startPos;
        }

        set
        {
            startPos = value;
        }
    }

    //Inicia onde ele estiver
    private void Awake()
    {
        
        GameObject finalpointCriate = new GameObject("FinalPointTarget");
        finalPointTarget = finalpointCriate.transform;
        GetComponent<PlayerSyncObj_LocalPosition>().MyTransform = finalPointTarget;

        plProps = GetComponent<PlayerProperties>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        //finalPointTarget = transform.Find("CharDefaultPack/FinalPointTarget").transform;

        if (transform.tag == "Blue_Side")
        {
            startPos = gameManager.playerStartPoints_BlueSide[0];
        }
        else if (transform.tag == "Red_Side")
        {
            startPos = gameManager.playerStartPoints_RedSide[0];
        }
    }

    private void Start()
    {
        

        transform.position = startPos.position;
        transform.rotation = startPos.rotation;

        transform.SetParent(GameObject.Find("CharsList/Players").transform);
        
        //PlayerManager.sPlayers.Add(this.gameObject);

        if (isLocalPlayer)
        {
            GameObject.Find("Scene Camera").SetActive(false);

            plProps.MainCamera.GetComponent<Camera>().enabled = true;
            plProps.MainCamera.GetComponent<AudioListener>().enabled = true;

            GetComponent<PlayerControllers>().enabled = true;
            GetComponent<PlayerMotor>().enabled = true;
            GetComponent<Player_UIManager>().enabled = true;
            GetComponent<CameraController>().enabled = true;
            GetComponent<GetFlag>().enabled = true;
            GetComponent<PlayerProperties>().enabled = true;
            GetComponent<PerFrameRayCast>().enabled = true;


        }
    }
    

    


    
}
