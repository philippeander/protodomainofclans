using System;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class PlayerMotor : NetworkBehaviour {

	public enum ControlFeel{Stiff, Loose}
	public ControlFeel AimControlFeel = ControlFeel.Loose;
	//public ControlFeel MoveControlFeel = ControlFeel.Loose;

	[Space(10)]
	[Header("AIM")]
	[SerializeField]float m_MovingTurnSpeed = 360;
	[SerializeField]float m_angleGeneralRotation = 45f;

	[Space(10)]
	[Header("MOVE")]
	[SerializeField] float m_MoveSpeedMultiplier = 2f;
	[SerializeField] float m_StationaryTurnSpeed = 180;
	[SerializeField] float m_JumpPower = 12f;
	[Range(1f, 4f)][SerializeField] float m_GravityMultiplier = 2f;
	[SerializeField] float m_GroundCheckDistance = 0.1f;

	[Space(10)]
	
	//[SerializeField]float debugRotAngle;
	//[SerializeField]bool debugBoolAngle;

	bool m_IsGrounded;
	bool idle;

	float m_OrigGroundCheckDistance;
	float m_TurnAmount = 0f;
	float m_ForwardAmount = 0f;

	Player_NetworkSetup player_networkSetup;
    PlayerProperties plProps;
	Rigidbody m_Rigidbody;
	//CapsuleCollider m_Capsule;
	Animator _animator;

    //Vector3 m_GroundNormal;
	Vector3 movement; 
	Vector3 newMove;
	Vector3 relative;

	[HideInInspector]public float aimTension = 20;

	public float SpeedMovement{get{return m_MoveSpeedMultiplier;}set{m_MoveSpeedMultiplier = value;}}

	void Awake(){
		m_Rigidbody = GetComponent<Rigidbody>();
        plProps = GetComponent<PlayerProperties>();
		_animator = GetComponent<Animator> ();
        player_networkSetup = GetComponent<Player_NetworkSetup>();
    }

	void Start()
	{
		m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
		m_OrigGroundCheckDistance = m_GroundCheckDistance;
        

    }


	public void Move(Vector3 move, bool jump)
	{
        if (!isLocalPlayer) return;

		// convert the world relative moveInput vector into a local-relative
		// turn amount and forward amount required to head in the desired
		// direction.
		Aim ();
		CheckGroundStatus();
		VerifyCrosshairAngle ();
		//debugBoolAngle = VerifyCrosshairAngle ();
		MecanimParameters ();

		if (move.magnitude > 1f) move.Normalize();

		Vector3 input = new Vector3(move.x * m_MoveSpeedMultiplier, 0f, move.z * m_MoveSpeedMultiplier);

		Vector3 movement = transform.TransformDirection  (input);

		m_Rigidbody.MovePosition (transform.position + (movement * 0.01f));

		relative = transform.InverseTransformDirection (movement);

		//Checks if it is idle to perform status actions
		if (relative.x == 0f && relative.z == 0f) {
			idle = true;
		} else {
			idle = false;
		}

//		//Se tiver Um player controlando, usa-se a primeira parte do codigo
//		//Se o personagem é controlado por AI ou se ocorrer algum problema, usa-se a segunda parte 
//		if (GetComponent<PlayerController> ().enabled) {
//			movement = new Vector3 (move.x * m_MoveSpeedMultiplier, 0f, move.z* m_MoveSpeedMultiplier);
//			movement = transform.TransformDirection (movement);
//			m_Rigidbody.MovePosition (transform.position + movement * 0.01f);
//
//			ApplyExtraTurnRotation ();
//		} else {
//			newMove = move;
//			newMove = transform.InverseTransformDirection(newMove);
//			newMove = Vector3.ProjectOnPlane(newMove, m_GroundNormal);
//			if(newMove != Vector3.zero){
//				m_TurnAmount = Mathf.Atan2 (newMove.x, newMove.z);
//				m_ForwardAmount = newMove.z;
//				ApplyExtraTurnRotation ();
//			}
//			movement.Set (move.x, 0f, move.z);
//			movement = movement * m_MoveSpeedMultiplier * Time.fixedDeltaTime;
//			m_Rigidbody.MovePosition (m_Rigidbody.position + movement);
//		}

		// control and velocity handling is different when grounded and airborne:
		if (m_IsGrounded)
		{
			HandleGroundedMovement(jump);
		}
		else
		{	
			HandleAirborneMovement();
		}
			
	}
    
	void Aim(){

		Vector3 dir = player_networkSetup.FinalPointTarget.position - transform.position;
		dir.y = 0f;
		Quaternion fin = Quaternion.LookRotation (dir);

		if(AimControlFeel == ControlFeel.Stiff){
			float angle = Quaternion.Angle (transform.rotation, fin);
			float derp = angle / m_MovingTurnSpeed / aimTension;
			float progress = Mathf.Min (1f, Time.deltaTime / derp);

			//If it is idle the angle is greater than the given angle
			if (idle && VerifyCrosshairAngle ()) {
				transform.rotation = Quaternion.Slerp (transform.rotation, fin, progress);
			}else if(!idle){
				transform.rotation = Quaternion.Slerp (transform.rotation, fin, progress);
			}
		}
		else if(AimControlFeel == ControlFeel.Loose){
			//If it is idle the angle is greater than the given angle
			if (idle && VerifyCrosshairAngle ()) {
				transform.rotation = Quaternion.Slerp (transform.rotation, fin,
					m_MovingTurnSpeed * Time.deltaTime);
			} else if (!idle) {
				transform.rotation = Quaternion.Slerp (transform.rotation, fin,
					m_MovingTurnSpeed * Time.deltaTime);
			}

		}
	}

	bool VerifyCrosshairAngle(){
		Vector3 targetDir = player_networkSetup.FinalPointTarget.position - transform.position;
		float angle = Vector3.Angle (targetDir, transform.forward);
		//debugRotAngle = angle;
		if (angle > m_angleGeneralRotation) {
			return true;
		} else {
			return false;
		}
	}

	void ApplyExtraTurnRotation()
	{
		if (GetComponent<PlayerControllers> ().enabled) {
			Vector3 targetPos = plProps.MainCamera.GetComponent<Camera>().ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
			targetPos.y = transform.position.y;
			Quaternion targetDir = Quaternion.LookRotation (transform.position - targetPos);
			transform.rotation = Quaternion.Slerp (transform.rotation, targetDir, m_MovingTurnSpeed * Time.fixedDeltaTime);
		} else {
			float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
			transform.Rotate(0, m_TurnAmount * turnSpeed * Time.fixedDeltaTime, 0);
		}
	}


	void HandleAirborneMovement()
	{
		// apply extra gravity from multiplier:
		Vector3 extraGravityForce = (Physics.gravity * m_GravityMultiplier) - Physics.gravity;
		m_Rigidbody.AddForce(extraGravityForce);

		m_GroundCheckDistance = m_Rigidbody.velocity.y < 0 ? m_OrigGroundCheckDistance : 0.01f;
	}


	void HandleGroundedMovement(bool jump)
	{
		// check whether conditions are right to allow a jump:
		if (jump)
		{
			// jump!
			m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
			//m_IsGrounded = false;
			m_GroundCheckDistance = 0.1f;
		}
	}

	void CheckGroundStatus()
	{
		RaycastHit hitInfo;

		// helper to visualise the ground check ray in the scene view

		// 0.1f is a small offset to start the ray from inside the character
		// it is also good to note that the transform position in the sample assets is at the base of the character
		if (Physics.Raycast(new Vector3(transform.position.x, transform.position.y + 0.01f, transform.position.z), Vector3.down, out hitInfo, m_GroundCheckDistance))
		{
			m_IsGrounded = true;
			//m_GroundNormal = hitInfo.normal;
		}
		else
		{
			m_IsGrounded = false;
			//m_GroundNormal = Vector3.up;
		}

		Debug.DrawRay(transform.position, Vector3.down * m_GroundCheckDistance, Color.red);

	}

	public void MecanimParameters()
	{
		//Sends the animation parameters in the Animator

		_animator.SetFloat ("Turn", relative.x);
		_animator.SetFloat ("Forward", relative.z);
		_animator.SetBool ("OnAir", m_IsGrounded);
		//_animator.SetBool ("Dead", health.Dead);

//		if(plKnife != null)
//			_animator.SetBool ("Attack", plKnife.OnKnife);

	}
}
