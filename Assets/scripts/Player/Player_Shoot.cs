﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_Shoot : NetworkBehaviour
{

    [Space(15)]
    [Header("BULLET OBJECT POOLING")]
    [SerializeField] float bulletDamage = 15;
    [SerializeField] float bulletSpeed = 30;

    [Space(10)]
    [SerializeField]Transform[] spownerPoint;

    string bulletTag;
    string enemyTag;

    CharTags charProp;

    public NetworkedPool spawnManager;
    
    void Start()
    {
        
        charProp = GetComponent<CharTags>();

        bulletTag = charProp.MyBulletTag;
        enemyTag = charProp.EnemyTag;

        //spawnManager = GameObject.Find("BulletPool_PlayerPassiva").GetComponent<NetworkedPool>();

    }



    //---------------- RECEBE OS VALORES DO INPUT no "Player Controlers"---------------------------

    private void Update()
    {
        if (spawnManager == null && GameObject.Find("BulletPool_PlayerPassiva"))
        {
            spawnManager = GameObject.Find("BulletPool_PlayerPassiva").GetComponent<NetworkedPool>();
            
        }
        
    }

    public void ShootEnemy()
    {
        if (spawnManager == null) return;
        if (!hasAuthority) return; 

        CmdIsShoot();
        
    }
   
    [Command]
    void CmdIsShoot()
    {
        for (int i = 0; i < spownerPoint.Length; i++)
        {
            //Quaternion rot = Quaternion.LookRotation(GetComponent<Player_NetworkSetup>().FinalPointTarget.position - transform.position);

            GameObject l_Bullet = spawnManager.GetFromPool(spownerPoint[i].position);

            //l_Bullet.transform.rotation = rot;
            l_Bullet.transform.LookAt(GetComponent<Player_NetworkSetup>().FinalPointTarget.position);

            l_Bullet.tag = bulletTag;
            
            CmdSetBulletParamiters(l_Bullet);

            l_Bullet.GetComponent<Rigidbody>().velocity = l_Bullet.transform.forward * bulletSpeed;
            
            NetworkServer.Spawn(l_Bullet, spawnManager.assetId);

            //StartCoroutine(Destroy(l_Bullet, 2.0f));
        }

    }
    
    [Command]
    void CmdSetBulletParamiters(GameObject bullet)
    {
        BulletLife BL = bullet.GetComponent<BulletLife>();

        BL.ObjQueDisparou = this.gameObject;
        BL.EnemyTag = enemyTag;
        BL.BulletValue = bulletDamage;
    }

    /*
    public IEnumerator Destroy(GameObject go, float timer)
    {
        yield return new WaitForSeconds(timer);
        spawnManager.UnSpawnObject(go);
        NetworkServer.UnSpawn(go);
    }
    */
}

