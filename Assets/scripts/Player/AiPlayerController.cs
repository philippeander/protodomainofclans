﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using System;

public class AiPlayerController : Sensores {
	[Space(10)]
	[Header("AI PLAYER CONTROLLER")]
	[SerializeField] float minWayPointDistance = 2f;
	[SerializeField]float MovingTurnSpeed = 400.0f;
	//[SerializeField]float timeToShoot = 0.5f;
	//[SerializeField]float timeToAttack = 1.5f;
	//[SerializeField]float distMinionsWave = 15.0f;

	NavMeshAgent nav;
	PlayerMotor character;
	WayPoints waypoint;
	[SerializeField]Transform[] waypoints;
	[SerializeField]GameObject[] companions = new GameObject[0];
	//PlayerPassiva playerPassiva;
	Health playerHealth;
	//Transform minion;
	Vector3 initialPosition;
	int laneNumber;
	[SerializeField]int currentWaypoint = 0;
	int maxWaypoint;
	bool inversePath = false;
	//int verifyPoint = 0;
	//bool shootPassiva = false;
	bool criticalState = false;


	public int LaneNumber{get{return laneNumber;}set{laneNumber = value;}}
	public bool InversePath{get{return inversePath;}set{inversePath = value;}}

	void OnDisable(){
		currentWaypoint = 0; 
	}

	public override void Awake (){
		base.Awake ();

		nav = GetComponent<NavMeshAgent> ();
		character = GetComponent<PlayerMotor> ();
		waypoint = GameObject.Find ("Lanes").GetComponent<WayPoints>();
//		playerPassiva = GetComponent<PlayerPassiva> ();
		playerHealth = GetComponent<Health> ();

	}


	public override void Start () {
		base.Start ();

		currentWaypoint = 0;
		nav.updateRotation = false;
		nav.updatePosition = true;
		initialPosition = this.transform.position;

		if (laneNumber == 1 && !inversePath) waypoints = waypoint.GroupPoints [0].points;
		else if (laneNumber == 1 && inversePath) waypoints = waypoint.GroupPoints [1].points;
		else if (laneNumber == 2 && !inversePath) waypoints = waypoint.GroupPoints [2].points;
		else if (laneNumber == 2 && inversePath) waypoints = waypoint.GroupPoints [3].points;
		else if (laneNumber == 3 && !inversePath) waypoints = waypoint.GroupPoints [4].points;
		else if (laneNumber == 3 && inversePath) waypoints = waypoint.GroupPoints [5].points;
		else waypoints = waypoint.GroupPoints [0].points;
		maxWaypoint = waypoints.Length; 

		int l_cont = 1;
		for(int i = 0; i < gameManager.ListOfAllChar.Count; i++){
			if(gameManager.ListOfAllChar[i].CompareTag(transform.tag)){
				Array.Resize (ref companions, l_cont);
				companions[l_cont - 1] = gameManager.ListOfAllChar[i];
				l_cont++;
			}
		}

		InvokeRepeating ("AutoShootPassiva", 0.5f, 0.5f);
	}
	

	public override void Update () {
		base.Update ();
		//SensorMinionsWave ();
		NavManager ();

		if (nav.remainingDistance > nav.stoppingDistance) {
			character.Move (nav.desiredVelocity, false);
		} else {
			character.Move (Vector3.zero, false);
		}
	}

	public override void FixedUpdate(){
		base.FixedUpdate ();

	}

	void NavManager()
	{
		
		Vector3 tempLocalPos;
		Vector3 tempWaypointPos;

		if(playerHealth.LifeCurrent < playerHealth.Life_MaxValue / 2){
			criticalState = true;
		}else{
			criticalState = false;
		}

		tempLocalPos = new Vector3 (transform.position.x, 0f, transform.position.z);
		tempWaypointPos = new Vector3 (waypoints[currentWaypoint].position.x, 0f, waypoints[currentWaypoint].position.z);
		//Aqui ele apenas seta o cuurentWayPoint, e não o destino
		if(Vector3.Distance(tempWaypointPos, tempLocalPos) <= minWayPointDistance && !criticalState){
			if (currentWaypoint == maxWaypoint) {
				currentWaypoint = 0;
			} else {
				currentWaypoint++;
			}
		}


		if (!criticalState) {
			//Path Patrulha
			if (targetSensorArea == null) { 
				nav.isStopped = false;
				nav.SetDestination (waypoints [currentWaypoint].position);
			}
			// Persue Enemy
			else if (targetSensorArea != null && targetToShoot == null && targetToAttack == null) {
                nav.isStopped = false;
                nav.SetDestination (targetSensorArea.position);
			}

			//Attacks Passiva
			else if (targetToShoot != null ) {
                nav.isStopped = true;
                ApllyExtraRotation (targetToShoot.position);
				//if (inSight) {
					//shootPassiva = true;
				//} 
			} else if (targetToAttack != null) {
                nav.isStopped = true;
                ApllyExtraRotation (targetToAttack.position);
				if (inSight) {
					//set animation
					print ("Puff Puff");
				} 
			}
		}
		//Ai Damage
		else{
            nav.isStopped = false; ;
			nav.SetDestination (initialPosition);
		}

		//shootPassiva = false;
		//playerHealth.CarregandoLifeBase = false;
	}

	void AutoShootPassiva(){
		//playerPassiva.GetInput (shootPassiva);
	}

	void ApllyExtraRotation(Vector3 target){
		Vector3 targetPos = target;
		targetPos.y = transform.position.y;
		Quaternion targetDir = Quaternion.LookRotation (targetPos - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, targetDir, MovingTurnSpeed * Time.fixedDeltaTime);
	}
    /*
	void SensorMinionsWave(){
		float shortesDistance = Mathf.Infinity;
		GameObject nearestEnemy = null;

		for(int i = 0; i < companions.Length; i++){
			if (companions[i].activeSelf) {
				float distanceToEnemy = Vector3.Distance (transform.position, companions[i].transform.position);
				if (distanceToEnemy < shortesDistance) {
					shortesDistance = distanceToEnemy;
					nearestEnemy = companions[i];
				}
			}
		}
        
		//Posso replicar esse meto e as unicas variaveis serem ajustadas são:
		if (nearestEnemy != null && shortesDistance <= distMinionsWave) 	//<<<
			minion = nearestEnemy.transform;	//<<<
		else
			minion = null; //<<<	
            
    }
    */
}
