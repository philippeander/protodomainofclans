﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class GetFlag : NetworkBehaviour {

	[SerializeField] GameObject pointFlag; 	//Variavel para pegar o ponto onde o player vai carregar a bandeira e parentea-la
	[SerializeField] Text textInfos;		//Texto com informação de ações

	GameObject tempFlag;					//é setada apenas quando o player está sobre a bandeira, e serve para mostrar a variavel Flag* qual objeto sera setado permanentemente	
	GameObject Flag;						//Variavel permanente para setar a bandeira
	bool IsOverFlag;						//Verifica se o player está sobre o gatilho  da bandeira
	bool withFlag;							//verifica se o player está carregando a bandeira
	bool playerDied; 						//Avisa que o player morreu para que a bandeira seja solta naquele mesmo local

    GameManager gameManager;

	//Propriedade, parafazer o player soltar a bandeira quando ele morrer
	public bool PlayerDied{get{return playerDied;}set{playerDied = value;}} 

	void Awake(){
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        textInfos = gameManager.TxtInfos;
	}

	void Start () {
		IsOverFlag = false;
		withFlag = false;
		playerDied = false;
	}

	void Update () {
        if (!isLocalPlayer) return;

        FlagActions ();
	}

	void OnTriggerEnter(Collider other) {
        if (!isLocalPlayer) return; 
		if(other.gameObject.tag == "Flag_Blue" || other.gameObject.tag == "Flag_Red")
        {
			textInfos.text = "Press \"E\" to pick up or drop the flag";
			IsOverFlag = true;				//variavel para verificar junto com a acão INPUT  em outros Merodos
			tempFlag = other.gameObject;	
		}
	}
	void OnTriggerExit(Collider other) {

        if (!isLocalPlayer) return;

        if (other.gameObject.tag == "Flag_Blue" || other.gameObject.tag == "Flag_Blue")
        {
			textInfos.text = "";
			IsOverFlag = false;
			tempFlag = null;
		}
	}

	void FlagActions(){
		if(!withFlag && IsOverFlag && Input.GetKeyUp(KeyCode.E)){
			textInfos.text = "";
			Flag = tempFlag;
			IsOverFlag = false;
			withFlag = true;

			Flag.transform.SetParent (pointFlag.transform);
			Flag.transform.position = pointFlag.transform.position;
			Flag.GetComponent<Collider> ().enabled = false;
			Flag.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
			Flag.transform.localRotation = Quaternion.Euler (0, 190, 0); 

		}
		else if(withFlag && Input.GetKeyUp(KeyCode.E) || withFlag && playerDied){
			Flag.transform.SetParent (GameObject.Find("Flags").transform);
			Flag.transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
			Flag.GetComponent<Collider> ().enabled = true;
			Flag.transform.localScale = new Vector3 (1, 1, 1);


			withFlag = false;
			Flag = null;
			playerDied = false;
		}


	}


}
