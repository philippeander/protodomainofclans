﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySyncObj_Position : NetworkBehaviour {

    [SerializeField] private Transform obj;
    [SyncVar] private Vector3 syncPos;

    private Vector3 lastPos;
    private float lerpRate = 10;
    private float posThreshold = 0.5f;
    
    void Update()
    {
        TransmitMotion();
        LerpMotion();
    }

    void TransmitMotion()
    {
        if (!isServer)
        {
            return;
        }

        if (Vector3.Distance(obj.position, lastPos) > posThreshold)
        {
            lastPos = obj.position;

            syncPos = obj.position;
        }
    }

    void LerpMotion()
    {
        if (isServer)
        {
            return;
        }

        obj.position = Vector3.Lerp(obj.position, syncPos, Time.deltaTime * lerpRate);

    }
}
