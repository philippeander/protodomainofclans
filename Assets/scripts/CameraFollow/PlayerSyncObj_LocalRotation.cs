﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSyncObj_LocalRotation : NetworkBehaviour {

    [SyncVar(hook = "OnPlayerRotSynced")]Vector3 syncPlayerRotation;

    [SerializeField] Transform objTransform;
    private float lerpRate = 20;

    private Vector3 lastPlayerRot;
    private float threshold = 0.3f;

    private List<Vector3> syncPlayerRotList = new List<Vector3>();

    private float closeEnough = 0.4f;
    [SerializeField] private bool useHistoricalInterpolation;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        LerpRotations();
    }

    void FixedUpdate()
    {
        TransmitRotations();

    }

    void LerpRotations()
    {
        if (!isLocalPlayer)
        {
            if (useHistoricalInterpolation)
            {
                HistoricalInterpolation();
            }
            else
            {
                OrdinaryLerping();
            }
        }
    }

    void HistoricalInterpolation()
    {
        if (syncPlayerRotList.Count > 0)
        {
            LerpPlayerRotation(syncPlayerRotList[0]);

            if (Mathf.Abs(objTransform.localEulerAngles.x - syncPlayerRotList[0].x) < closeEnough||
                Mathf.Abs(objTransform.localEulerAngles.y - syncPlayerRotList[0].y) < closeEnough||
                Mathf.Abs(objTransform.localEulerAngles.z - syncPlayerRotList[0].z) < closeEnough)
            {
                syncPlayerRotList.RemoveAt(0);
            }

            //Debug.Log(syncPlayerRotList.Count.ToString() + " syncPlayerRotList Count");
        }
    }

    void OrdinaryLerping()
    {
        LerpPlayerRotation(syncPlayerRotation);
    }

    void LerpPlayerRotation(Vector3 rotAngle)
    {
        Vector3 playerNewRot = new Vector3(rotAngle.x, rotAngle.y, rotAngle.z);
        objTransform.localRotation = Quaternion.Lerp(objTransform.localRotation, Quaternion.Euler(playerNewRot), lerpRate * Time.deltaTime);
    }

    //-----------------------------------------------------------

    [Client]
    void TransmitRotations()
    {
        if (isLocalPlayer)
        {
            if (CheckIfBeyondThreshold(objTransform.localEulerAngles, lastPlayerRot))
            {
                lastPlayerRot = objTransform.localEulerAngles;
                CmdProvideRotationsToServer(lastPlayerRot);
            }
        }
    }

    bool CheckIfBeyondThreshold(Vector3 rot1, Vector3 rot2)
    {
        if (Mathf.Abs(rot1.x - rot2.x) > threshold ||
            Mathf.Abs(rot1.y - rot2.y) > threshold ||
            Mathf.Abs(rot1.z - rot2.z) > threshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [Command]
    void CmdProvideRotationsToServer(Vector3 playerRot)
    {
        syncPlayerRotation = playerRot;
        
    }

    [Client]
    void OnPlayerRotSynced(Vector3 latestPlayerRot)
    {
        syncPlayerRotation = latestPlayerRot;
        syncPlayerRotList.Add(syncPlayerRotation);
    }
}
