﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour {


	[SerializeField] Vector3 endPointRaycast;
	[SerializeField] float distRaycast = 20f;
	[SerializeField] Vector3 hitPoint;
	[SerializeField] float VisualRectExtraSize = 1.5f;
	[SerializeField] Image imgCrossHair;
	[SerializeField] Color color1 = Color.white;
	[SerializeField] Color color2 = Color.red;
	[Space(10)]
	[Header("UI SELECTION")]
	[SerializeField] GameObject selBox;
	[SerializeField] GameObject selectedObject;


	string enemyTag = "";
	//bool UiSellection = false;
	RaycastHit hitInfo;

	public Vector3 HitPoint{get{return hitPoint;}}
	public string EnemieTag{get{return enemyTag;}set{enemyTag = value;}}
			
	void Start(){
		selBox = GameObject.Find ("UiSelectionIndicator");
	}

	void Update(){
		UiSelectionBox ();
	}

	void FixedUpdate () {

		int x = Screen.width / 2;
		int y = Screen.height / 2;
		Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y));
		Debug.DrawRay(ray.origin, ray.direction * distRaycast, Color.blue);
		endPointRaycast = ray.origin + (ray.direction * distRaycast) ;
		if (Physics.Raycast (ray, out hitInfo, distRaycast)) {
			if (hitInfo.collider) {
				hitPoint = hitInfo.point;
			}

			if (hitInfo.collider.CompareTag(enemyTag)) {
				imgCrossHair.color = color2;
				selectedObject = hitInfo.transform.gameObject;
			} else {
				imgCrossHair.color = color1;
				selectedObject = null;
			}

		} else {
			hitPoint = endPointRaycast;
			imgCrossHair.color = color1;
			selectedObject = null;

		}
	}

	void UiSelectionBox(){
		if (selectedObject != null) {
			
			for (int i = 0; i < selBox.transform.childCount; i++) {
				selBox.transform.GetChild (i).gameObject.SetActive (true);
			}

			Rect visualRect = RendererBoundsInScreenSpace (selectedObject.GetComponentInChildren<Renderer> ());
			RectTransform rt = selBox.GetComponent<RectTransform> ();
			rt.position = new Vector2 (visualRect.xMin, visualRect.yMin);
			rt.sizeDelta = new Vector2 (visualRect.width*VisualRectExtraSize, visualRect.height*VisualRectExtraSize);
		} else {
			for (int i = 0; i < selBox.transform.childCount; i++) {
				selBox.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
	}

	static Vector3[] screenSpaceCorners;
	static Rect RendererBoundsInScreenSpace(Renderer r){
		Bounds bigBounds = r.bounds;

		if(screenSpaceCorners == null){
			screenSpaceCorners = new Vector3[8];
		}

		Camera theCamera = Camera.main;

		// For each of the 8 corners of our renderer's world space bounding box,
		// convert those corners into screen space.//para cada 8 cantos de um redenrer em world Space
		screenSpaceCorners[0] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x + bigBounds.extents.x, bigBounds.center.y + bigBounds.extents.y, bigBounds.center.z + bigBounds.extents.z ) );
		screenSpaceCorners[1] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x + bigBounds.extents.x, bigBounds.center.y + bigBounds.extents.y, bigBounds.center.z - bigBounds.extents.z ) );
		screenSpaceCorners[2] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x + bigBounds.extents.x, bigBounds.center.y - bigBounds.extents.y, bigBounds.center.z + bigBounds.extents.z ) );
		screenSpaceCorners[3] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x + bigBounds.extents.x, bigBounds.center.y - bigBounds.extents.y, bigBounds.center.z - bigBounds.extents.z ) );
		screenSpaceCorners[4] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x - bigBounds.extents.x, bigBounds.center.y + bigBounds.extents.y, bigBounds.center.z + bigBounds.extents.z ) );
		screenSpaceCorners[5] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x - bigBounds.extents.x, bigBounds.center.y + bigBounds.extents.y, bigBounds.center.z - bigBounds.extents.z ) );
		screenSpaceCorners[6] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x - bigBounds.extents.x, bigBounds.center.y - bigBounds.extents.y, bigBounds.center.z + bigBounds.extents.z ) );
		screenSpaceCorners[7] = theCamera.WorldToScreenPoint( new Vector3( bigBounds.center.x - bigBounds.extents.x, bigBounds.center.y - bigBounds.extents.y, bigBounds.center.z - bigBounds.extents.z ) );

		// Now find the min/max X & Y of these screen space corners.
		float min_x = screenSpaceCorners [0].x;
		float min_y = screenSpaceCorners [0].y;
		float max_x = screenSpaceCorners [0].x;
		float max_y = screenSpaceCorners [0].y;

		for(int i = 1; i < 8; i++){
			if (screenSpaceCorners [i].x < min_x)
				min_x = screenSpaceCorners [i].x;
			if (screenSpaceCorners [i].y < min_y)
				min_y = screenSpaceCorners [i].y;
			if (screenSpaceCorners [i].x > max_x)
				max_x = screenSpaceCorners [i].x;
			if (screenSpaceCorners [i].y > max_y)
				max_y = screenSpaceCorners [i].y;
		}

		return Rect.MinMaxRect (min_x, min_y, max_x, max_y);


	}
		
}
