﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemySyncObj_Rotation : NetworkBehaviour {

    [SerializeField] private Transform obj;
    [SyncVar] private Vector3 syncYRot;
    
    private Quaternion lastRot;
    private float lerpRate = 20;
    private float rotThreshold = 0.05f;
    
	void Update () {
        
        LerpMotion();
    }

    private void FixedUpdate()
    {
        TransmitMotion();
    }

    void TransmitMotion()
    {
        if (!isServer) return;
        

        if (CheckIfBeyondThreshold(obj.localRotation, lastRot))
        {
            lastRot = obj.localRotation;
            syncYRot = obj.localEulerAngles;
        }
    }

    bool CheckIfBeyondThreshold(Quaternion rot1, Quaternion rot2)
    {
        if (Mathf.Abs(rot1.x - rot2.x) > rotThreshold ||
            Mathf.Abs(rot1.y - rot2.y) > rotThreshold ||
            Mathf.Abs(rot1.z - rot2.z) > rotThreshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void LerpMotion()
    {
        if (isServer) return;
        
        
        Vector3 newRot = new Vector3(syncYRot.x, syncYRot.y, syncYRot.z);
        obj.localRotation = Quaternion.Lerp(obj.localRotation, Quaternion.Euler(newRot), Time.deltaTime * lerpRate);
    }
}
