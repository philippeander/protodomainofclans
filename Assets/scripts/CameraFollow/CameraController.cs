﻿using System;
using UnityEngine;
using System.Collections;
//using UnityEngine.Networking;

public class CameraController : MonoBehaviour {

    
	[SerializeField] Transform target; 
	[SerializeField] float distance = 3.0f;
	[SerializeField] float minAngleY = -60.0f;
	[SerializeField] float maxAngleY = 30.0f;

	Transform camTransform;
	float currentX = 0.0f;
	float currentY = 0.0f;
    //float sensivityX = 4.0f;
    //float sensivityY = 1.0f;
    
    PlayerControllers plCtrls;
    
    private void Awake()
    {
        camTransform = transform.Find("CharDefaultPack/MainCamera").gameObject.transform;
        target = transform.Find("CharDefaultPack/CamTarget").transform;
        plCtrls = GetComponent<PlayerControllers>();

    }

    // Use this for initialization
    void Start () {
		
		

        //Cursor.lockState = CursorLockMode.Locked;
        //Cursor.visible = true;

    }
	
	// Update is called once per frame
	void LateUpdate () {

        if (camTransform == null) return;
        if (target == null) return;

		Vector3 dir = new Vector3 (0,0,-distance);
		Quaternion rotation = Quaternion.Euler (-currentY,currentX,0);
		camTransform.position = target.position + rotation * dir;
		camTransform.LookAt (target.position);
	}

	void Update(){
		currentX += plCtrls.MouseMoviment.x;
		currentY += plCtrls.MouseMoviment.y;

		currentY = Mathf.Clamp (currentY, minAngleY, maxAngleY );
	}
}
