﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSyncObj_LocalPosition : NetworkBehaviour {

    [SyncVar(hook = "SyncPositionValues")]
    private Vector3 syncPos;

    [SerializeField] Transform myTransform;
    private float lerpRate;
    private float normalLerpRate = 16;
    private float fasterLerpRate = 27;

    private Vector3 lastPos;
    private float threshold = 0.5f;

    //private NetworkClient nClient;

    private List<Vector3> syncPosList = new List<Vector3>();
    [SerializeField] private bool useHistoricalLerping = false;
    private float closeEnough = 0.11f;

    public Transform MyTransform
    {
        get
        {
            return myTransform;
        }

        set
        {
            myTransform = value;
        }
    }

    void Start () {
        //nClient = GameObject.Find("NetworkManager").GetComponent<NetworkManager>().client;
        lerpRate = normalLerpRate;
    }
	
	// Update is called once per frame
	void Update () {
        LerpPosition();
    }

    void FixedUpdate()
    {
        TransmitPosition();

    }

    void LerpPosition()
    {
        if (!isLocalPlayer)
        {
            if (useHistoricalLerping)
            {
                HistoricalLerping();
            }
            else
            {
                OrdinaryLerping();
            }

            //Debug.Log(Time.deltaTime.ToString());
        }
    }

    [Command]
    void CmdProvidePositionToServer(Vector3 pos)
    {
        syncPos = pos;
        //Debug.Log("Command called");
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer && Vector3.Distance(myTransform.localPosition, lastPos) > threshold)
        {
            CmdProvidePositionToServer(myTransform.localPosition);
            lastPos = myTransform.localPosition;
        }
    }

    [Client]
    void SyncPositionValues(Vector3 latestPos)
    {
        syncPos = latestPos;
        syncPosList.Add(syncPos);
    }



    void OrdinaryLerping()
    {
        myTransform.localPosition = Vector3.Lerp(myTransform.localPosition, syncPos, Time.deltaTime * lerpRate);
    }

    void HistoricalLerping()
    {
        if (syncPosList.Count > 0)
        {
            myTransform.localPosition = Vector3.Lerp(myTransform.localPosition, syncPosList[0], Time.deltaTime * lerpRate);

            if (Vector3.Distance(myTransform.localPosition, syncPosList[0]) < closeEnough)
            {
                syncPosList.RemoveAt(0);
            }

            if (syncPosList.Count > 10)
            {
                lerpRate = fasterLerpRate;
            }
            else
            {
                lerpRate = normalLerpRate;
            }

            //Debug.Log(syncPosList.Count.ToString());
        }
    }
}
