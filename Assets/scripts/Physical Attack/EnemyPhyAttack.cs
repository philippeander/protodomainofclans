﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnemyPhyAttack : NetworkBehaviour
{

    [SerializeField] float damageByAttack;

    [Space(10)]
    [SerializeField] GameObject[] weaponSlot;
    [SerializeField] List<Collider> collDamage;
    [SyncVar][SerializeField] bool m_Attack;
    [SerializeField] bool multipleAttack = false;
    string enemyTag;

    Animator anim;
    CharTags charProp;

    void Awake()
    {
        anim = GetComponent<Animator>();

        for (int i = 0; i < weaponSlot.Length; i++)
        {
            collDamage.Add(weaponSlot[i].GetComponentInChildren<SphereCollider>());
        }

    }

    void OnEnable()
    {


    }

    // Use this for initialization
    void Start()
    {
        m_Attack = false;
        charProp = GetComponent<CharTags>();
        enemyTag = charProp.EnemyTag;

        for (int i = 0; i < collDamage.Count; i++)
        {
            PhyAttackDemage phyDamage = collDamage[i].GetComponent<PhyAttackDemage>();

            phyDamage.Damage = damageByAttack;
            phyDamage.EnemyTag = enemyTag;
            phyDamage.FatherObject = this.gameObject;

            collDamage[i].enabled = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!isServer) return;

        Anim();
    }

    public void AttackEnemy()
    {

        m_Attack = true;
    }

    void Anim()
    {
        if (anim)
        {
            anim.SetBool("Attack", m_Attack);
            if (m_Attack && multipleAttack)
                anim.SetInteger("AttackType", UnityEngine.Random.Range(0, 2));
        }
    }

    //Metodo chamado no "animation event"
    void ActiveCollider()
    {
        for (int i = 0; i < collDamage.Count; i++)
        {
            collDamage[i].enabled = true;
        }
    }

    void DeactiveCollider()
    {
        for (int i = 0; i < collDamage.Count; i++)
        {
            collDamage[i].enabled = false;
        }

        m_Attack = false;
    }
    
}
