﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhyAttackDemage : MonoBehaviour {

    [SerializeField]float damage = 25;
    [SerializeField] bool onTrigger = false;
    [SerializeField] string enemyTag;
    [SerializeField] GameObject fatherObject;

    public float Damage { set { damage = value; } }
    public string EnemyTag { set { enemyTag = value; } }
    public GameObject FatherObject { set { fatherObject = value; } }

    private void Update()
    {
        if (GetComponent<Collider>().enabled == true) { return; }

        if (GetComponent<Collider>().enabled == false) {
            onTrigger = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Punch " + other.name);
        if (onTrigger) return;

        if (other.tag == enemyTag) {
            Health health = other.GetComponent<Health>();
            if (health) {
                onTrigger = true;
                health.OnDamage(damage, fatherObject);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == enemyTag)
        {
            onTrigger = false;
        }
    }

}
