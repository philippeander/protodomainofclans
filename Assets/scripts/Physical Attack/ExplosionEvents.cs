﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEvents : MonoBehaviour {
    [SerializeField] float explosionDelay = 0f;
    [SerializeField] float blastRadius = 10f;
    [SerializeField] float explosionPower = 200f;
    [SerializeField] float damageMinArea = 50f;
    [SerializeField] float damageMaxArea = 25f;
    [SerializeField] float minDamageArea = 4f;
    [SerializeField] float maxDamageArea = 8f;
    [SerializeField] LayerMask explosionLayers;
    [SerializeField] GameObject objFather;
    [SerializeField] string enemyTag;

    bool dontReturn = false;
    
    Collider[] hitCollides;

    public LayerMask ExplosionLayers
    {
        get
        {
            return explosionLayers;
        }

        set
        {
            explosionLayers = value;
        }
    }
    public GameObject ObjFather
    {
        get
        {
            return objFather;
        }

        set
        {
            objFather = value;
        }
    }
    public string EnemyTag
    {
        get
        {
            return enemyTag;
        }

        set
        {
            enemyTag = value;
        }
    }

    void OnEnable()
    {
        dontReturn = false;
        StartCoroutine(IsExplosion());
    }
    
    IEnumerator IsExplosion()
    {

        yield return new WaitForSeconds(explosionDelay);

        if (!dontReturn)
        {
            hitCollides = Physics.OverlapSphere(transform.position, blastRadius, explosionLayers);

            for (int i = 0; i < hitCollides.Length; i++)
            {
                if (hitCollides[i].GetComponent<Rigidbody>() != null)
                {
                    hitCollides[i].GetComponent<Rigidbody>().AddExplosionForce(explosionPower, transform.position, blastRadius, 1, ForceMode.Impulse);
                }

                if (hitCollides[i].gameObject.GetComponent<Health>())
                {
                    float dist = Vector3.Distance(transform.position, hitCollides[i].gameObject.transform.position);

                    if (dist <= minDamageArea)
                    {
                        hitCollides[i].gameObject.GetComponent<Health>().OnDamage(damageMinArea, objFather);
                    }
                    else if (dist <= maxDamageArea && dist > minDamageArea)
                    {
                        hitCollides[i].gameObject.GetComponent<Health>().OnDamage(damageMaxArea, objFather);
                    }
                }
                
            }
            dontReturn = true;
        }
    }



    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, blastRadius);
        Gizmos.DrawWireSphere(transform.position, minDamageArea);
        Gizmos.DrawWireSphere(transform.position, maxDamageArea);

    }
}
