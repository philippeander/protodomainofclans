﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PhysicalAttack : NetworkBehaviour {

    [SerializeField] float damageByAttack;

    [Space(10)]
    [SerializeField] GameObject[] weaponSlot; 
	[SerializeField]List<Collider> collDamage;
	[SyncVar] bool m_Attack;
    string enemyTag;

	Animator anim;
    CharTags charProp;

    void Awake(){
		anim = GetComponent<Animator> ();

        for (int i = 0; i < weaponSlot.Length; i++) {
            collDamage.Add(weaponSlot[i].GetComponentInChildren<SphereCollider>());
        }

	}

	void OnEnable(){
		

	}

	// Use this for initialization
	void Start () {
        m_Attack = false;
        charProp = GetComponent<CharTags>();
        enemyTag = charProp.EnemyTag;

        for (int i = 0; i < collDamage.Count; i++)
        {
            collDamage[i].GetComponent<PhyAttackDemage>().Damage = damageByAttack;
            collDamage[i].GetComponent<PhyAttackDemage>().EnemyTag = enemyTag;
            collDamage[i].GetComponent<PhyAttackDemage>().FatherObject = this.gameObject;

            collDamage[i].enabled = false;
        }

    }

    // Update is called once per frame
    void Update () {
        if (!isLocalPlayer) return;

		Anim ();
	}

	public void AttackEnemy(){

		m_Attack = true;
	}

	void Anim(){
		if(anim){
			anim.SetBool ("Attack", m_Attack);
		}
	}

	//Metodo chamado no "animation event"
	void ActiveCollider(){
		for(int i = 0; i < collDamage.Count; i++){
			collDamage [i].enabled = true;
		}
	}

	void DeactiveCollider(){
		for(int i = 0; i < collDamage.Count; i++){
			collDamage [i].enabled = false;
		}

		m_Attack = false;
	}

}
