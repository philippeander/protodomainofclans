﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Prototype.NetworkLobby
{
    public class MenuManager : NetworkBehaviour {
        
        public LobbyManager lobbyManager;
        public RectTransform charSelect;

        Animator anim;
        

        bool MatchMaker;
        bool DirectPlay;
        bool ServerList;
        
        private void Awake()
        {
            anim = GetComponent<Animator>();
            
        }


        private void Start()
        {
            MatchMaker = false;
            DirectPlay = false;
            ServerList = false;
        }

        private void Update()
        {
            Anim();


        }

        void Anim() {
            anim.SetBool("MatchMaker", MatchMaker);
            anim.SetBool("DirectPlay", DirectPlay);
            anim.SetBool("ServerList", ServerList);
        }

        public void OnMatch() {
            if (DirectPlay) DirectPlay = false;
            MatchMaker = !MatchMaker;
            if (!MatchMaker) {
                ServerList = false;
                lobbyManager.GoBackButton();
            }
        }

        public void OnDirectPlay() {
            if (MatchMaker) MatchMaker = false;
            if (ServerList) ServerList = false;
            DirectPlay = !DirectPlay;
        }

        public void OnServerList()
        {
            ServerList = MatchMaker ? true : false;
        }

        //============= PERSONAGENS =============================

        //host / servidor está no comando, isso significa que o anfitrião lhe diz qual personagem você está indo jogar.
        public void ChoseChar(int id) {

            //LobbyManager.s_Singleton.gamePlayerPrefab = LobbyManager.s_Singleton.spawnPrefabs[id];
            //LobbyManager.s_Singleton.SetPlayerTypeLobby(id);
            
        }

        public void OnPlayClick() {
            charSelect.gameObject.SetActive(false);
        }

        //====================================================

    }

}
