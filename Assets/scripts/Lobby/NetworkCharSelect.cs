﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Prototype.NetworkLobby
{
    public class NetworkCharSelect : MonoBehaviour
    {
        
        [SerializeField]LobbyPlayer lobbyPlayer;

        public LobbyPlayer MyLobbyPlayer
        {
            get
            {
                return lobbyPlayer;
            }

            set
            {
                lobbyPlayer = value;
            }
        }
        

        public void ChoseChar(int id)
        {
            if (lobbyPlayer)
                lobbyPlayer.OnCharIndexChanged(id);
        }

        public void OnPlayClick()
        {
            this.gameObject.SetActive(false);
        }
    }
}
