﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prototype.NetworkLobby;
using UnityEngine.Networking;

public class NetworkLobbyHook : LobbyHook
{

    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        if (lobbyPlayer == null) return;

        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();

        if (lobby != null) { 
            
            CharTags charTags = gamePlayer.GetComponent<CharTags>();
            Player_ID pl_ID = gamePlayer.GetComponent<Player_ID>();

            pl_ID.playerUniqueIdentity = lobby.playerName;
            //pl_ID.SetStringName(lobby.playerName);

            //charTags.myTag = lobby.playerTag_Current;
        }
    }
}
