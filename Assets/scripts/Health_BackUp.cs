﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Health_BackUp : NetworkBehaviour {

    //Valures que incrementam os valores Fixos nas atualizações
    [Header("UPDATE VALUES")]
    [SerializeField]
    float lifeUpdate = 20f;
    [SerializeField] float energyUpdate = 20f;
    [SerializeField] float resurrectUpdate = 6f;

    //Valores fixos
    [Space(10)]
    [Header("FIXED VALUES")]
    [SerializeField]
    [SyncVar]
    float life_MaxValue = 100f;
    [SerializeField] [SyncVar] float energy_MaxValue = 100f;
    [SerializeField] [SyncVar] float resurrectMaxValue = 12;

    [Space(10)]
    [Header("CURRENT VALUES")]
    [SerializeField]
    [SyncVar]
    float lifeCurrent;
    [SerializeField] [SyncVar] float energyCurrent;
    [SerializeField] [SyncVar] float resurrectCountDown;

    [Space(10)]
    [Header("CONDITIONS")]
    [SerializeField]
    bool isEvolves = false;    //Se o personagem evolui, a medida que mata
    [SerializeField] bool haveEnergy = false;   //Se o Char perde energia ao usar uma ataque
    [SerializeField] bool regenerate = false;   //Regenera o Life e Aenergia
    [SerializeField] bool invincible = false;   //Se perde life quando atacado
    [SerializeField] bool DamageFeed = false;   //Se spawna o valor do Golpe quando leva dano
    [SerializeField] bool returnToInitialPos = false;
    //[SerializeField] bool isMinion = false;

    [Space(10)]
    [Header("OBJECTS")]
    [SerializeField]
    GameObject DamageFeed_Prefab;

    //public delegate void DieDelegate();
    //public event DieDelegate EventDie;

    //public delegate void RespawnDelegate();
    //public event RespawnDelegate EventRespawn;

    CharTags charProp;
    Vector3 initialPos;
    GameObject baseSide;
    GameObject enemyLastAttack;

    string baseTag;

    [SyncVar] int playerLevel = 0; //Level do Player, evolução

    bool isDead;
    bool isBase;
    bool carregandoLifeBase;

    public float LifeCurrent
    {
        get
        {
            return lifeCurrent;
        }

        set
        {
            lifeCurrent = value;
        }
    }
    public float EnergyCurrent
    {
        get
        {
            return energyCurrent;
        }

        set
        {
            energyCurrent = value;
        }
    }
    public float Life_MaxValue
    {
        get
        {
            return life_MaxValue;
        }

        set
        {
            life_MaxValue = value;
        }
    }
    public float Energy_MaxValue
    {
        get
        {
            return energy_MaxValue;
        }

        set
        {
            energy_MaxValue = value;
        }
    }
    public bool IsDead
    {
        get
        {
            return isDead;
        }

        set
        {
            isDead = value;
        }
    }
    public bool IsEvolves
    {
        get
        {
            return isEvolves;
        }

        set
        {
            isEvolves = value;
        }
    }
    public bool CarregandoLifeBase
    {
        get
        {
            return carregandoLifeBase;
        }

        set
        {
            carregandoLifeBase = value;
        }
    }
    public int PlayerLevel
    {
        get
        {
            return playerLevel;
        }

        set
        {
            playerLevel = value;
        }
    }

    public float ResurrectMaxValue
    {
        get
        {
            return resurrectMaxValue;
        }

        set
        {
            resurrectMaxValue = value;
        }
    }
    public float ResurrectCountDown
    {
        get
        {
            return resurrectCountDown;
        }

        set
        {
            resurrectCountDown = value;
        }
    }

    public float LifeUpdate
    {
        get
        {
            return lifeUpdate;
        }

        set
        {
            lifeUpdate = value;
        }
    }

    public float EnergyUpdate
    {
        get
        {
            return energyUpdate;
        }

        set
        {
            energyUpdate = value;
        }
    }

    public float ResurrectUpdate
    {
        get
        {
            return resurrectUpdate;
        }

        set
        {
            resurrectUpdate = value;
        }
    }

    private void Awake()
    {
        if (DamageFeed)
            DamageFeed_Prefab = (GameObject)Resources.Load("LifeExtract_Feed", typeof(GameObject));

        charProp = GetComponent<CharTags>();

    }

    private void Start()
    {
        lifeCurrent = life_MaxValue;
        energyCurrent = energy_MaxValue;
        resurrectCountDown = resurrectMaxValue;
        initialPos = transform.position;
        isDead = false;
        isBase = false;
        carregandoLifeBase = false;

        if (regenerate)
            baseSide = GameObject.FindGameObjectWithTag(charProp.MyBaseTag);

    }

    public void RegenetareAndUpdateValues()
    {
        lifeCurrent = Life_MaxValue;
        energyCurrent = energy_MaxValue;
        resurrectMaxValue += ResurrectUpdate;
        resurrectCountDown = resurrectMaxValue;
        isDead = false;
    }

    private void Update()
    {

        if (!isServer) return;

        if (regenerate)
        {
            BaseDistanceToRegenerate();
            LifeAddByTime();
            if (haveEnergy)
                EnergyAddByTime();
        }

        if (lifeCurrent > life_MaxValue)
        {
            lifeCurrent = life_MaxValue;
        }

        if (energyCurrent > energy_MaxValue)
        {
            energyCurrent = energy_MaxValue;
        }


    }

    //Chamado pelo Script que difere o Attack
    public void OnDamage(float amount, GameObject objQueDisparou)
    {
        if (!isServer) return;
        if (invincible) return;
        if (isDead) return;
        if (amount <= 0) return;

        enemyLastAttack = objQueDisparou;
        lifeCurrent -= amount;

        if (DamageFeed)
        {
            GameObject instFeed = Instantiate(DamageFeed_Prefab, new Vector3(transform.position.x, transform.position.y + 1.8f, transform.position.z), Quaternion.identity, gameObject.transform) as GameObject;
            instFeed.GetComponentInChildren<TextMesh>().text = amount.ToString();
            Destroy(instFeed, 2f);

        }

        if (lifeCurrent <= 0)
        {
            RpcWhenDie();

        }

        enemyLastAttack = null;

    }

    //Quando isDead = true
    //Aumenta a vida e Energia do Inimigo
    //Incrementa O level do Player que atacou
    [ClientRpc]
    void RpcWhenDie()
    {

        if (isLocalPlayer) // <<<<<<<<<
        {
            isDead = true;

            if (returnToInitialPos)
                transform.position = initialPos;

            //Parte responsavel pelo incremento do Tempo de Ressussitação do Player apos Morrer
            Health_BackUp healthEnemy = enemyLastAttack.GetComponent<Health_BackUp>();
            if (healthEnemy)
            {
                if (healthEnemy.IsEvolves)
                {
                    
                    healthEnemy.PlayerLevel += 1;
                    healthEnemy.Life_MaxValue += healthEnemy.LifeUpdate;
                    healthEnemy.Energy_MaxValue += healthEnemy.EnergyUpdate;
                    
                }
            }
        }
    }

    void LifeAddByTime()
    {
        if (lifeCurrent < life_MaxValue && lifeCurrent > 1 && isBase)
        {           //Se o o player estiver na Base

            lifeCurrent += Time.deltaTime * 5f;
        }
        else if (lifeCurrent < life_MaxValue && lifeCurrent > 1 && !isBase)
        {   //Se o player "NÃO" estiver na Base
            lifeCurrent += Time.deltaTime / 2.5f;

        }
    }
    void EnergyAddByTime()
    {
        if (energyCurrent < energy_MaxValue && energyCurrent > 1)
        {
            if (isBase)
            {           //Se o o player estiver na Base
                energyCurrent += Time.deltaTime * 5f;
            }
            else
            {   //Se o player "NÃO" estiver na Base
                energyCurrent += Time.deltaTime / 2.5f;
            }
        }
    }
    void BaseDistanceToRegenerate()
    {
        if (regenerate)
        {
            if (Vector3.Distance(transform.position, baseSide.transform.position) <= baseSide.GetComponent<BaseRecover>().RadiusArea)
            {
                isBase = true;
                if (lifeCurrent < life_MaxValue)
                {
                    carregandoLifeBase = true;
                }
            }
            else
            {
                isBase = false;
                if (lifeCurrent >= life_MaxValue)
                {
                    carregandoLifeBase = false;
                }
            }
        }
    }


    //ATENÇÃO: A EXEMPLO DA TORRE, é ela que fica com esse metodo, e o sensor dela que pega o player para playerToRecover
    public void RecoverByInput(GameObject playerToRecover)
    {//Pode ser um met publico para ser acessado pela classe principal

        Health playerHealth = playerToRecover.GetComponent<Health>();

        if (Input.GetKey(KeyCode.R))
        {

            if (playerHealth.LifeCurrent > 0)
            {
                float val = Time.deltaTime / 2f;

                this.lifeCurrent += val; //Aqui é o life da torre

                playerHealth.OnDamage(val, null);

            }

        }

    }
}
