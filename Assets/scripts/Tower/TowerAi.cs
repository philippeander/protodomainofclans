﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class TowerAi : Sensores {

	[Header("GUN MOVIMENT")]
	[SerializeField]GameObject Turret;
	[SerializeField]GameObject weaponSlot;
	[SerializeField]GameObject lights;
	[SerializeField]GameObject shells;
	[SerializeField]float m_MovingTurnSpeed = 100.0f;

    [SyncVar] Vector3 targetPos;
    [SyncVar] Vector3 barrelLookPos;

    Health towerHealth;
	Shot shooting;

	//Sensores sensores = new Sensores();

	public Transform TargetSensorArea{get{return targetSensorArea;}}

	public override void Awake () {
		base.Awake ();

		shooting = GetComponent<Shot> ();
		towerHealth = GetComponent<Health> ();

	}

	public override void Start(){
		base.Start ();

	}
	

	public override void Update () {
        base.Update ();

		GunTurn ();
		RecoverTower ();
		PartsRotation ();

	}

	public override void FixedUpdate(){
		base.FixedUpdate ();

	}

	//Ele verifica se tem algum jogador com a teg Char, na area de recuperação da torre
	void RecoverTower(){
		if (targetCharacters != null) {
			towerHealth.RecoverByInput (targetCharacters.gameObject);
		}
	}
		
	//Telacionado a arma no topo da torre
	void GunTurn(){

		if(targetSensorArea != null && !towerHealth.ShouldDie){
            
			targetPos = targetSensorArea.position - Turret.transform.position;
			targetPos.y = 0f;
			Quaternion targetDir = Quaternion.LookRotation (targetPos);
			Turret.transform.rotation = Quaternion.Slerp (Turret.transform.rotation, targetDir, Time.deltaTime * m_MovingTurnSpeed);

			barrelLookPos = (new Vector3(targetSensorArea.position.x, targetSensorArea.position.y + 0.5f, targetSensorArea.position.z)) - weaponSlot.transform.position;
            //barrelLookPos.y = 0f;
			Quaternion barrelDir = Quaternion.LookRotation (barrelLookPos);
			weaponSlot.transform.rotation = Quaternion.Slerp (weaponSlot.transform.rotation, barrelDir, Time.deltaTime * m_MovingTurnSpeed);

			shooting.ShootEnemy (finalPoint);
		}
	}

	void PartsRotation(){

		lights.transform.RotateAround(lights.transform.position, lights.transform.up, Time.deltaTime * 90f);
		shells.transform.RotateAround(shells.transform.position, shells.transform.up, Time.deltaTime * -90f);
	}


	//Faz o gerenciamento das ballas

}
