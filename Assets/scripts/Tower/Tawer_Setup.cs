﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Tawer_Setup : NetworkBehaviour {

	void Start () {
        if (isServer)
        {
            GetComponent<TowerAi>().enabled = true;
            GetComponent<Shot>().enabled = true;
        }
    }
	
}
