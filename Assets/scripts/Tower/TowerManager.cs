﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class TowerManager : NetworkBehaviour {

    [Space(10)]
    [Header("TAWERs CONFIG.")]
    [SerializeField]GameObject BlueTowerPrefab;
	[SerializeField]GameObject redTowerPrefab;
	[SerializeField]Transform[] towerSpawnerPoints_Blue_Side = null;
	[SerializeField]Transform[] towerSpawnerPoints_Red_Side = null;
	
    
    public override void OnStartServer()
    {
        TowerInit();
    }


    void TowerInit(){
		
        for (int i = 0; i < towerSpawnerPoints_Blue_Side.Length; i++)
        {
            SpawnTowers(BlueTowerPrefab, towerSpawnerPoints_Blue_Side[i]);
        }

        for (int i = 0; i < towerSpawnerPoints_Red_Side.Length; i++)
        {
            SpawnTowers(redTowerPrefab, towerSpawnerPoints_Red_Side[i]);
        }
    }

    void SpawnTowers(GameObject prefab, Transform spawnPos)
    {
        GameObject instTawer = Instantiate(prefab, spawnPos.position, spawnPos.rotation) as GameObject;
        instTawer.transform.parent = transform;

        NetworkServer.Spawn(instTawer);
    }
    
}
