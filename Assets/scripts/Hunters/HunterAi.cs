﻿using UnityEngine;
using System.Collections;
using System;
using System.Globalization;
using UnityEngine.AI;
using UnityEngine.Networking;

public class HunterAi : Sensores{

	enum HunterAttackType{Shoot, PhysicalAttack}


	[Space(15)]
	[Header("HUNTER AI")]
	[SerializeField] HunterAttackType hunterAttackType;
	[SerializeField] float minWayPointDistance = 0.1f;
	[SerializeField]float MovingTurnSpeed = 360.0f;

	//Sensores sensores = new Sensores ();
	Animator anim;
	NavMeshAgent nav;
	NavMeshObstacle navObs;
	public WayPoints waypoint;
	Transform[] waypoints;
	public Shot hunterShoot;
	public EnemyPhyAttack physicalAttack;
	int CurrentWaypoint = 0;
	int maxWaypoint;
	int laneNumber;
	bool inversePath;
	float curSpeed;
	Vector3 previousPos;

	public int LaneNumber{get{return laneNumber;}set{laneNumber = value;}}
	public bool InversePath{get{return inversePath;}set{inversePath = value;}}

	void OnDisable(){
		CurrentWaypoint = 0;
	}

	public override void Awake () {
		base.Awake ();


		nav = GetComponent<NavMeshAgent> ();
		navObs = GetComponent<NavMeshObstacle> ();
		waypoint = GameObject.Find ("GameManager/Lanes").GetComponent<WayPoints>();
		anim = GetComponent<Animator> ();

        

        if (GetComponent<Shot> ()){
			hunterShoot = GetComponent<Shot> ();
				
		}
		if(GetComponent<EnemyPhyAttack>()){
			physicalAttack = GetComponent<EnemyPhyAttack> ();
		}
	}

	public override void Start(){
		base.Start ();

        if (laneNumber == 1 && !inversePath) waypoints = waypoint.GroupPoints[0].points;
        else if (laneNumber == 1 && inversePath) waypoints = waypoint.GroupPoints[1].points;
        else if (laneNumber == 2 && !inversePath) waypoints = waypoint.GroupPoints[2].points;
        else if (laneNumber == 2 && inversePath) waypoints = waypoint.GroupPoints[3].points;
        else if (laneNumber == 3 && !inversePath) waypoints = waypoint.GroupPoints[4].points;
        else if (laneNumber == 3 && inversePath) waypoints = waypoint.GroupPoints[5].points;
        else Debug.Log("The path is not definid on HunterAi! ");
		maxWaypoint = waypoints.Length;

        navObs.enabled = false;
        nav.enabled = true;
    }

	public override void Update () {
		base.Update ();

		NavManager ();
		Anim ();
	}
	public override void FixedUpdate(){
		base.FixedUpdate ();

	}

	void NavManager(){

		Vector3 curMove = transform.position - previousPos;
		curSpeed = curMove.magnitude / Time.deltaTime;
		previousPos = transform.position;

		//Rssa parte inteira esta relacionada ao Path
		Vector3 tempLocalPosition;
		Vector3 tempWaypointPosition;

		tempLocalPosition = new Vector3(transform.position.x, 0f, transform.position.z);
		tempWaypointPosition = new Vector3( waypoints[CurrentWaypoint].position.x, 0f, waypoints[CurrentWaypoint].position.z);

		// se ele chega a distancia minima do waypoint atual ele adiciona mais um
		if(Vector3.Distance(tempWaypointPosition, tempLocalPosition) <= minWayPointDistance){
			if (CurrentWaypoint == maxWaypoint)
				CurrentWaypoint = 0;
			else
				CurrentWaypoint++;
		}


		if (targetSensorArea != null && targetToAttack == null && targetToShoot == null) {
			//nav.Resume ();
			navObs.enabled = false;
			nav.enabled = true;
			nav.SetDestination (targetSensorArea.position);
		} else if (hunterAttackType == HunterAttackType.PhysicalAttack && targetToAttack != null) {
			//nav.Stop ();
			nav.enabled = false;
			navObs.enabled = true;
			ApllyExtraRotation (targetToAttack.position);
			if (inSight && physicalAttack) {
				physicalAttack.AttackEnemy ();
			}
		} else if (hunterAttackType == HunterAttackType.Shoot && targetToShoot != null) {
			//nav.Stop ();
			nav.enabled = false;
			navObs.enabled = true;
			ApllyExtraRotation (targetToShoot.position);
			if (inSight && hunterShoot) {
				hunterShoot.ShootEnemy (finalPoint);
			}
		} else {
			//nav.Resume ();
			navObs.enabled = false;
			nav.enabled = true;
			nav.SetDestination (waypoints[CurrentWaypoint].position);
		}
	}


	void ApllyExtraRotation(Vector3 target){
		Vector3 targetPos = target;
		targetPos.y = transform.position.y;
		Quaternion targetDir = Quaternion.LookRotation (targetPos - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, targetDir, MovingTurnSpeed * Time.fixedDeltaTime);
	}

	void Anim(){
		anim.SetFloat ("Move", curSpeed);
	}

}
