﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class HunterManager : NetworkBehaviour {
    

    [SerializeField] float timeBetweenWaves = 30.0f;
    [SerializeField] int numByWave = 6;
    
	[Space(10)]
	[Header("POINTS SPAWN")]
	[SerializeField]Transform pointsInst_Blue_Side;
	[SerializeField]Transform pointsInst_Red_Side;

    [Space(10)]
    [Header("NET POOL")]
    public NetworkedPool minionShoot_BlueSide;
    public NetworkedPool minionShoot_RedSide;
    public NetworkedPool minionAttack_BlueSide;
    public NetworkedPool minionAttack_RedSide;


    public bool spownWave = true;

	float countDownWave = 2f;

    private void Start()
    {
        if (numByWave % 2 != 0) numByWave += 1;
    }

    public override void OnStartServer()
    {
        GetNetPool();
    }



    void Update () {

        if (!isServer) return;
        

        if (spownWave && countDownWave >= 0) {
            countDownWave -= Time.deltaTime;
            if (countDownWave <= 0){
			    StartCoroutine (SpownWave());
			    countDownWave = timeBetweenWaves;
		    }
        }

	}

	IEnumerator SpownWave(){

        
        //Ele irá spawnar metade do numero total de minions, para ficar metade Attack e metade Shoot
        for (int j = 0; j < numByWave / 2; j++)
        {
            GameObject min_Blue = minionAttack_BlueSide.GetFromPool(pointsInst_Blue_Side.position);
            min_Blue.GetComponent<Minion_Dead>().ObjManager = minionAttack_BlueSide;
            min_Blue.GetComponent<HunterAi>().LaneNumber = 1;
            min_Blue.GetComponent<HunterAi>().InversePath = false;

            GameObject min_Red = minionAttack_RedSide.GetFromPool(pointsInst_Red_Side.position);
            min_Red.GetComponent<Minion_Dead>().ObjManager = minionAttack_RedSide;
            min_Red.GetComponent<HunterAi>().LaneNumber = 1;
            min_Red.GetComponent<HunterAi>().InversePath = true;

            NetworkServer.Spawn(min_Blue, minionAttack_BlueSide.assetId);
            NetworkServer.Spawn(min_Red, minionAttack_RedSide.assetId);

            yield return new WaitForSeconds(0.5f);
        }

        for (int j = 0; j < numByWave / 2; j++)
        {
            GameObject min_Blue = minionShoot_BlueSide.GetFromPool(pointsInst_Blue_Side.position);
            min_Blue.GetComponent<Minion_Dead>().ObjManager = minionShoot_BlueSide;
            min_Blue.GetComponent<HunterAi>().LaneNumber = 1;
            min_Blue.GetComponent<HunterAi>().InversePath = false;

            GameObject min_Red = minionShoot_RedSide.GetFromPool(pointsInst_Red_Side.position);
            min_Red.GetComponent<Minion_Dead>().ObjManager = minionShoot_RedSide;
            min_Red.GetComponent<HunterAi>().LaneNumber = 1;
            min_Red.GetComponent<HunterAi>().InversePath = true;

            NetworkServer.Spawn(min_Red, minionShoot_RedSide.assetId);
            NetworkServer.Spawn(min_Blue, minionShoot_BlueSide.assetId);

            yield return new WaitForSeconds(0.5f);
        }

    }

    void GetNetPool() {

        if(!minionShoot_BlueSide)
            minionShoot_BlueSide = GameObject.Find("EnemyPool_MinionShoot_Blue").GetComponent<NetworkedPool>();

        if (!minionShoot_RedSide)
            minionShoot_RedSide = GameObject.Find("EnemyPool_MinionShoot_Red").GetComponent<NetworkedPool>();

        if (!minionAttack_BlueSide)
            minionAttack_BlueSide = GameObject.Find("EnemyPool_MinionAttack_Blue").GetComponent<NetworkedPool>();

        if (!minionAttack_RedSide)
            minionAttack_RedSide = GameObject.Find("EnemyPool_MinionAttack_Red").GetComponent<NetworkedPool>();

    }

}
