﻿using UnityEngine;
using System.Collections;
using System;

public class WayPoints : MonoBehaviour {

	[Serializable]
	public class GroupPoint
	{
		[Header("LANE PATH")]
		public int lineNumber = 0;
		public GameObject groupFolder;
		public Transform[] points;
	}

	[SerializeField] GroupPoint[] groupPoints = null;

	public GroupPoint[] GroupPoints{get{return groupPoints;}}

//	void Awake(){
//		foreach(GroupPoint GP in groupPoints){
//			GP.points = new Transform[GP.groupFolder.transform.childCount];
//			for(int i = 0; i < GP.points.Length; i++){
//				GP.points [i] = GP.groupFolder.transform.GetChild (i);
//			}
//		}
//	}
}
