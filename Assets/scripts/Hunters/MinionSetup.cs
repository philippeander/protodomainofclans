﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MinionSetup : NetworkBehaviour {
    
    void Start () {
        if (isServer)
        {
            GetComponent<HunterAi>().enabled = true;

            if(GetComponent<Shot>())
                GetComponent<Shot>().enabled = true;
        }
	}
	
}
