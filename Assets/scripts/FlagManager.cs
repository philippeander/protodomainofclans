﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public class FlagManager : NetworkBehaviour {

    string base_Blue_Side = "Base_Blue_Side";
    string base_Red_Side = "Base_Red_Side";

    string flag_Blue = "Flag_Blue";
    string flag_Red = "Flag_Red";

    string enemyBaseTag;
    GameObject baseEnemy;
    float baseRadius;

    public GameObject gameOverScreen;
    public Text gameOverTxt;

    GameManager gameManager;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Use this for initialization
    void Start()
    {
        if (transform.tag == flag_Blue)
        {
            enemyBaseTag = base_Red_Side;
        }
        else if (transform.tag == flag_Red)
        {
            enemyBaseTag = base_Blue_Side;
        }
        else
        {
            Debug.Log("Falha em enemyBaseTag");
        }

        baseEnemy = GameObject.FindGameObjectWithTag(enemyBaseTag);
        baseRadius = baseEnemy.GetComponent<BaseRecover>().RadiusArea;

        gameOverScreen = gameManager.GameOverScreen;
        gameOverTxt = gameManager.GameOverTxt;

        InvokeRepeating("CheckBaseDistance", 0f, 1f);
    }
	
	void CheckBaseDistance()
    {
        if (Vector3.Distance(transform.position, baseEnemy.transform.position ) < baseRadius)
        {
            if(transform.tag == flag_Blue)
            {
                gameOverScreen.SetActive(true);
                gameOverTxt.text = "GAME OVER \nSIDE RED WIN...";
                StartCoroutine(ReloadGame());
            }
        }
    }

    IEnumerator ReloadGame()
    {
        yield return new WaitForSeconds(3f);
        Debug.Log("GAME FINISH");
        //SceneManager.LoadScene(0);
    } 
}
