﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseRecover : MonoBehaviour {

	[Range(1f, 15f)][SerializeField]float radiusArea = 14f;

	public float RadiusArea{get{return radiusArea;}}

	void OnDrawGizmosSelected(){
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere (transform.position, radiusArea);
	}

}
