﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CharTags : NetworkBehaviour {   

    [Space(10)]
    [Header("MY TAG PARAMTERS")]
    [SyncVar]public string myTag = "Blue_Side";
    [SerializeField] string myBulletTag;
    [SerializeField] string myBaseTag;
    [SerializeField] int myLayerMask;

    [Space(10)]
    [Header("ENEMY TAG PARAMTERS")]
    [SerializeField] string enemyTag;
    [SerializeField] string enemyBulletTag;
    [SerializeField] string enemyBaseTag;
    [SerializeField] int enemyLayerMask;

    GameManager gameManager;

    public string MyTag
    {
        get
        {
            return myTag;
        }

        set
        {
            myTag = value;
        }
    }
    public string MyBulletTag
    {
        get
        {
            return myBulletTag;
        }

        set
        {
            myBulletTag = value;
        }
    }
    public string MyBaseTag
    {
        get
        {
            return myBaseTag;
        }

        set
        {
            myBaseTag = value;
        }
    }
    public int MyLayerMask
    {
        get
        {
            return myLayerMask;
        }

        set
        {
            myLayerMask = value;
        }
    }
    //-----------------------------------
    public string EnemyTag
    {
        get
        {
            return enemyTag;
        }

        set
        {
            enemyTag = value;
        }
    }
    public string EnemyBulletTag
    {
        get
        {
            return enemyBulletTag;
        }

        set
        {
            enemyBulletTag = value;
        }
    }
    public string EnemyBaseTag
    {
        get
        {
            return enemyBaseTag;
        }

        set
        {
            enemyBaseTag = value;
        }
    }
    public int EnemyLayerMask
    {
        get
        {
            return enemyLayerMask;
        }

        set
        {
            enemyLayerMask = value;
        }
    }



    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        Init();
    }

    private void Update()
    {
        //Init();
    }

    void Init()
    {
        //Setar os ListOfAllChar aqui, por que essa é uma classe que está presente em todos os personagens 
        gameManager.ListOfAllChar.Add(this.gameObject);

        if (transform.tag == "Blue_Side")
        {

            gameManager.SetCharProperties_Blue_Side(this);

            this.gameObject.transform.tag = myTag;
        }
        else if (transform.tag == "Red_Side")
        {

            gameManager.SetCharProperties_Red_Side(this);

            this.gameObject.transform.tag = myTag;
        }
    }
}
